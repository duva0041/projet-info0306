package com.example.info0306_projet_antoine_duval.page2.dresseur;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.SecondActivity;

import java.util.Objects;

public class EvoliActivity extends AppCompatActivity {
    private int numeroPierre = 0;

    private static MediaPlayer cri;
    private static MediaPlayer evolutionSon;

    private TextView txtEvoli;

    private ImageView imgEvoli;

    private ImageView pierreEau;
    private ImageView pierreFeu;
    private ImageView pierreFoudre;
    private ImageView pierrePlante;
    private ImageView pierreGlace;
    private ImageView pierreLune;
    private ImageView pierreNuit;
    private ImageView pierreEclat;

    private TextView txtPierre;


    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoli);

        //Mise en page
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        //bruitage lors de l'evolution
        evolutionSon = MediaPlayer.create(EvoliActivity.this, R.raw.capture);

        //texte titre
        txtEvoli = findViewById(R.id.textEvolutionEvoli);

        //image d'evoli
        imgEvoli = findViewById(R.id.imageEvoli);
        imgEvoli.setOnDragListener(this::onDrag);

        //Les pierres
        pierreEau = findViewById(R.id.imgPierreEau);
        pierreEau.setOnTouchListener(this::onTouch);
        pierreEau.setTag("DRAGGABLE PIERRE EAU");

        pierreFeu = findViewById(R.id.imgPierreFeu);
        pierreFeu.setOnTouchListener(this::onTouch);
        pierreFeu.setTag("DRAGGABLE PIERRE FEU");

        pierreFoudre = findViewById(R.id.imgPierreFoudre);
        pierreFoudre.setOnTouchListener(this::onTouch);
        pierreFoudre.setTag("DRAGGABLE PIERRE FOUDRE");

        pierrePlante = findViewById(R.id.imgPierrePlante);
        pierrePlante.setOnTouchListener(this::onTouch);
        pierrePlante.setTag("DRAGGABLE PIERRE PLANTE");

        pierreGlace = findViewById(R.id.imgPierreGlace);
        pierreGlace.setOnTouchListener(this::onTouch);
        pierreGlace.setTag("DRAGGABLE PIERRE GLACE");

        pierreLune = findViewById(R.id.imgPierreLune);
        pierreLune.setOnTouchListener(this::onTouch);
        pierreLune.setTag("DRAGGABLE PIERRE LUNE");

        pierreNuit = findViewById(R.id.imgPierreNuit);
        pierreNuit.setOnTouchListener(this::onTouch);
        pierreNuit.setTag("DRAGGABLE PIERRE NUIT");

        pierreEclat = findViewById(R.id.imgPierreEclat);
        pierreEclat.setOnTouchListener(this::onTouch);
        pierreEclat.setTag("DRAGGABLE PIERRE ECLAT");

        //texte de la pierre
        txtPierre = findViewById(R.id.textPierre);
        txtPierre.setVisibility(View.INVISIBLE);

        //Les boutons en bas
        ImageView imgRetour = findViewById(R.id.imgRetourEvoli);
        imgRetour.setOnClickListener(v -> goBack());

        ImageView imgRestart = findViewById(R.id.imgRestartEvolution);
        imgRestart.setOnClickListener(v -> restartEvolution());

    }

    public void cacherPierres() {
        pierreEau.setVisibility(View.INVISIBLE);
        pierreFeu.setVisibility(View.INVISIBLE);
        pierreFoudre.setVisibility(View.INVISIBLE);
        pierrePlante.setVisibility(View.INVISIBLE);
        pierreGlace.setVisibility(View.INVISIBLE);
        pierreLune.setVisibility(View.INVISIBLE);
        pierreNuit.setVisibility(View.INVISIBLE);
        pierreEclat.setVisibility(View.INVISIBLE);
    }

    public void afficherPierres() {
        pierreEau.setVisibility(View.VISIBLE);
        pierreFeu.setVisibility(View.VISIBLE);
        pierreFoudre.setVisibility(View.VISIBLE);
        pierrePlante.setVisibility(View.VISIBLE);
        pierreGlace.setVisibility(View.VISIBLE);
        pierreLune.setVisibility(View.VISIBLE);
        pierreNuit.setVisibility(View.VISIBLE);
        pierreEclat.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    public void evolution() {
        switch (numeroPierre) {
            case 1:
                txtEvoli.setText("Tu as maintenant un Aquali !");
                imgEvoli.setImageResource(R.drawable.aquali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.aquali);
                MainActivity.setSavePokemon(23);
                break;
            case 2:
                txtEvoli.setText("Tu as maintenant un Pyroli !");
                imgEvoli.setImageResource(R.drawable.pyroli);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.pyroli);
                MainActivity.setSavePokemon(24);
                break;
            case 3:
                txtEvoli.setText("Tu as maintenant un Voltali !");
                imgEvoli.setImageResource(R.drawable.voltali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.voltali);
                MainActivity.setSavePokemon(25);
                break;
            case 4:
                txtEvoli.setText("Tu as maintenant un Phyllali !");
                imgEvoli.setImageResource(R.drawable.phyllali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.phyllali);
                MainActivity.setSavePokemon(26);
                break;
            case 5:
                txtEvoli.setText("Tu as maintenant un Givrali !");
                imgEvoli.setImageResource(R.drawable.givrali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.givrali);
                MainActivity.setSavePokemon(27);
                break;
            case 6:
                txtEvoli.setText("Tu as maintenant un Noctali !");
                imgEvoli.setImageResource(R.drawable.noctali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.noctali);
                MainActivity.setSavePokemon(28);
                break;
            case 7:
                txtEvoli.setText("Tu as maintenant un Mentali !");
                imgEvoli.setImageResource(R.drawable.mentali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.mentali);
                MainActivity.setSavePokemon(29);
                break;
            case 8:
                txtEvoli.setText("Tu as maintenant un Nymphali !");
                imgEvoli.setImageResource(R.drawable.nymphali);
                cri = MediaPlayer.create(EvoliActivity.this, R.raw.nymphali);
                MainActivity.setSavePokemon(30);
                break;
        }

        if (MainActivity.getEffetSonore()) {
            cri.start();
        }
    }

    @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
    public boolean onTouch(View v, MotionEvent motionEvent) {
        MainActivity.effetPopup();
        ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(v.getTag().toString(), mimeTypes, item);
        View.DragShadowBuilder dragshadow = new View.DragShadowBuilder(v);
        v.startDrag(data, dragshadow, v, 0);
        cacherPierres();
        txtPierre.setVisibility(View.VISIBLE);

        switch (v.getId()){
            case R.id.imgPierreEau:
                txtPierre.setText("Pierre Eau");
                numeroPierre = 1;
                break;
            case R.id.imgPierreFeu:
                txtPierre.setText("Pierre Feu");
                numeroPierre = 2;
                break;
            case R.id.imgPierreFoudre:
                txtPierre.setText("Pierre Foudre");
                numeroPierre = 3;
                break;
            case R.id.imgPierrePlante:
                txtPierre.setText("Pierre Plante");
                numeroPierre = 4;
                break;
            case R.id.imgPierreGlace:
                txtPierre.setText("Pierre Glace");
                numeroPierre = 5;
                break;
            case R.id.imgPierreLune:
                txtPierre.setText("Pierre Lune");
                numeroPierre = 6;
                break;
            case R.id.imgPierreNuit:
                txtPierre.setText("Pierre Nuit");
                numeroPierre = 7;
                break;
            case R.id.imgPierreEclat:
                txtPierre.setText("Pierre Eclat");
                numeroPierre = 8;
                break;
        }

        return true;
    }

    @SuppressLint("SetTextI18n")
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:
                return event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            case DragEvent.ACTION_DRAG_ENTERED:

            case DragEvent.ACTION_DRAG_EXITED:

            case DragEvent.ACTION_DROP:
                v.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                v.invalidate();
                MainActivity.effetPopup();
                if (event.getResult()) {
                    cacherPierres();
                    evolution();
                    effetEvolution();
                    txtPierre.setVisibility(View.INVISIBLE);
                    txtPierre.setText("");
                }
                else {
                    afficherPierres();
                    txtPierre.setText("Recommence !");
                }
                return true;
            default:
                Log.e("PROJET LOG", "Erreur drag and drop");
                break;
        }
        return false;
    }

    public void effetEvolution() {
        if (MainActivity.getEffetSonore()) {
            evolutionSon.start();
        }
    }

    public void goBack() {
        MainActivity.effetPopup();
        Log.i("PROJET", "Page Dresseur");
        startActivity(new Intent(this, DresseurActivity.class));
        finish();
    }

    public void restartEvolution() {
        MainActivity.effetPopup();
        Log.i("PROJET", "Restart evoli");
        startActivity(new Intent(this, EvoliActivity.class));
        finish();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, DresseurActivity.class));
        finish();
    }
}