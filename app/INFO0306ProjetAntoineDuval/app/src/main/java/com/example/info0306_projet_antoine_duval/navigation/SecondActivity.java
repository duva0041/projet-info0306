package com.example.info0306_projet_antoine_duval.navigation;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.example.info0306_projet_antoine_duval.page2.dresseur.DresseurActivity;
import com.example.info0306_projet_antoine_duval.page2.FantominusActivity;
import com.example.info0306_projet_antoine_duval.page2.QuizzActivity;
import com.example.info0306_projet_antoine_duval.R;

import java.util.Objects;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_second);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        Log.i("PROJET", "Create page 2");
        //Toast.makeText(this, "Page 2", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET", "Start");
        /* Toast.makeText(this, "Start", Toast.LENGTH_SHORT).show(); */
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET", "Resume");
        /* Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET", "Pause");
        /* Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();  */
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET", "Stop");
        /* Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET", "Restart");
        /* Toast.makeText(this, "Restart", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET", "Destroy");
        /* Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET", "BackPressed");
        /* Toast.makeText(this, "BackPressed", Toast.LENGTH_SHORT).show(); */

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void goMain(View v)
    {
        Log.d("PROJET", "retour main");
        MainActivity.effetPopup();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void openActivityThree(View v)
    {
        Log.d("PROJET", "page 3");
        MainActivity.effetPopup();
        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    public void openFantominus(View button)
    {
        MainActivity.effetPopup();
        Log.i("PROJET", "open Fantominus");
        startActivity(new Intent(this, FantominusActivity.class));
        finish();
    }

    public void openQuizz(View button) {
        MainActivity.effetPopup();
        Log.i("PROJET", "open quizz");
        startActivity(new Intent(this, QuizzActivity.class));
        finish();
    }

    public void openDresseur(View button) {
        MainActivity.effetPopup();
        Log.i("PROJET", "open coin du dresseur");
        startActivity(new Intent(this, DresseurActivity.class));
        finish();
    }
}