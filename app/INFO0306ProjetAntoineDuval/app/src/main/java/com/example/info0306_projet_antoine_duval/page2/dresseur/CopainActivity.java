package com.example.info0306_projet_antoine_duval.page2.dresseur;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.page1.CaptureActivity;
import com.example.info0306_projet_antoine_duval.page1.DeoxysActivity;

import java.util.Objects;

public class CopainActivity extends AppCompatActivity {
    private String lienPokemon = "https://www.pokebip.com/";
    private static MediaPlayer bruitage;
    private int numeroSwitch;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copain);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_trainer);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#272727"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Poké-Dico</font>"));
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        switch (MainActivity.getPageCopain()) {
            case 3:
                numeroSwitch = MainActivity.getPokemonSauvage();
                break;
            case 4:
                numeroSwitch = MainActivity.getNumeroDeoxys();
                break;
            default:
                numeroSwitch = MainActivity.getSavePokemon();
                break;
        }

        affichagePage();

        ConstraintLayout layout = findViewById(R.id.layoutCopain);
        layout.setOnTouchListener(new OnSwipeTouchListener(CopainActivity.this) {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                Toast.makeText(CopainActivity.this, "Page suivante", Toast.LENGTH_SHORT).show();
                augmentation();
            }
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                Toast.makeText(CopainActivity.this, "Page précédente", Toast.LENGTH_SHORT).show();
                diminution();
            }
        });

    }

    public void augmentation() {
        switch (numeroSwitch) {
            case 3:
            case 6:
                numeroSwitch += 3;
                break;
            case 9:
            case 11:
            case 17:
            case 19:
                numeroSwitch += 2;
                break;
            case 34:
                numeroSwitch = 3;
                break;
            default:
                numeroSwitch++;
                break;
        }
        affichagePage();
    }

    public void diminution() {
        switch (numeroSwitch) {
            case 6:
            case 9:
                numeroSwitch -= 3;
                break;
            case 11:
            case 13:
            case 19:
            case 21:
                numeroSwitch -= 2;
                break;
            case 3:
                numeroSwitch = 34;
                break;
            default:
                numeroSwitch--;
                break;
        }
        affichagePage();
    }

    @SuppressLint("SetTextI18n")
    public void affichagePage() {
        ImageView imgPokemon = findViewById(R.id.imageDuPokemon);
        TextView txtPokemon = findViewById(R.id.nomDuPokemon);
        TextView txtNumero = findViewById(R.id.numeroDuPokemon);
        TextView txtType = findViewById(R.id.typeDuPokemon);
        TextView txtEspece = findViewById(R.id.especeDuPokemon);

        switch (numeroSwitch) {
            case 3:
                imgPokemon.setImageResource(R.drawable.pikachu);
                txtPokemon.setText("Pikachu");
                txtNumero.setText("Numéro : 025");
                txtType.setText("Type : Electrik");
                txtEspece.setText("Espèce : Souris");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/pikachu";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.pikachu);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.pikachu_shuffle);
                break;
            case 6:
                imgPokemon.setImageResource(R.drawable.evoli);
                txtPokemon.setText("Evoli");
                txtNumero.setText("Numéro : 133");
                txtType.setText("Type : Normal");
                txtEspece.setText("Espèce : Evolutif");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/evoli";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.evoli);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.evoli_shuffle);
                break;
            case 9:
                imgPokemon.setImageResource(R.drawable.blizzi);
                txtPokemon.setText("Blizzi");
                txtNumero.setText("Numéro : 142");
                txtType.setText("Type : Plante & Glace");
                txtEspece.setText("Espèce : Arbregelé");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/blizzi";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.blizzi);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.blizzi_shuffle);
                break;
            case 11:
                imgPokemon.setImageResource(R.drawable.carchacrok);
                txtPokemon.setText("Carchacrok");
                txtNumero.setText("Numéro : 445");
                txtType.setText("Dragon & Sol");
                txtEspece.setText("Type : Espèce : Supersonic");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/carchacrok";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.carchacrok);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.carchacrok_shuffle);
                break;
            case 13:
                imgPokemon.setImageResource(R.drawable.mega_tyrannocif);
                txtPokemon.setText("Mega - Tyranocif");
                txtNumero.setText("Numéro : 248");
                txtType.setText("Type : Roche & Ténèbres");
                txtEspece.setText("Espèce : Armure");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/mega-tyranocif";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.tyrannocif);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.mega_tyranocif_suffle);
                break;
            case 14:
                imgPokemon.setImageResource(R.drawable.groudon);
                txtPokemon.setText("Groudon");
                txtNumero.setText("Numéro : 383");
                txtType.setText("Type : Sol");
                txtEspece.setText("Espèce : Continent");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/groudon";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.groudon);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.groudon_shuffle);
                break;
            case 15:
                imgPokemon.setImageResource(R.drawable.kyogre);
                txtPokemon.setText("Kyogre");
                txtNumero.setText("Numéro : 382");
                txtType.setText("Type : Eau");
                txtEspece.setText("Espèce : Bassinmarin");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/kyogre";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.kyogre);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.kyogre_shuffle);
                break;
            case 16:
                imgPokemon.setImageResource(R.drawable.rayquaza);
                txtPokemon.setText("Rayquaza");
                txtNumero.setText("Numéro : 384");
                txtType.setText("Type : Dragon & Vol");
                txtEspece.setText("Espèce : Cieux");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/rayquaza";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.rayquaza);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.rayquaza_suffle);
                break;
            case 17:
                imgPokemon.setImageResource(R.drawable.evoli_shiny);
                txtPokemon.setText("Evoli (shiny)");
                txtNumero.setText("Numéro : 133");
                txtType.setText("Type : Normal");
                txtEspece.setText("Espèce : Evolutif");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/evoli";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.evoli);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.evoli_shiny_shuffle);
                break;
            case 19:
                imgPokemon.setImageResource(R.drawable.salameche);
                txtPokemon.setText("Salamèche");
                txtNumero.setText("Numéro : 004");
                txtType.setText("Type : Feu");
                txtEspece.setText("Espèce : Lézard");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/salameche";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.salameche);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.salameche_shuffle);
                break;
            case 21:
                imgPokemon.setImageResource(R.drawable.etourmi);
                txtPokemon.setText("Etourmi");
                txtNumero.setText("Numéro : 396");
                txtType.setText("Type : Normal & Vol");
                txtEspece.setText("Espèce : Etourneau");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/etourmi";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.etourmi);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.etourmi_shuffle);
                break;
            case 22:
                imgPokemon.setImageResource(R.drawable.metamorph);
                txtPokemon.setText("Métamorph");
                txtNumero.setText("Numéro : 132");
                txtType.setText("Type : Normal");
                txtEspece.setText("Espèce : Morphing");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/metamorph";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.metamorph);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.metamorph_shuffle);
                break;
            case 23:
                imgPokemon.setImageResource(R.drawable.aquali);
                txtPokemon.setText("Aquali");
                txtNumero.setText("Numéro : 134");
                txtType.setText("Type : Eau");
                txtEspece.setText("Espèce : Bulleur");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/aquali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.aquali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.aquali_shuffle);
                break;
            case 24:
                imgPokemon.setImageResource(R.drawable.pyroli);
                txtPokemon.setText("Pyroli");
                txtNumero.setText("Numéro : 136");
                txtType.setText("Type : Feu");
                txtEspece.setText("Espèce : Flamme");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/pyroli";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.pyroli);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.pyroli_shuffle);
                break;
            case 25:
                imgPokemon.setImageResource(R.drawable.voltali);
                txtPokemon.setText("Voltali");
                txtNumero.setText("Numéro : 135");
                txtType.setText("Type : Electrik");
                txtEspece.setText("Espèce : Orage");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/voltali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.voltali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.voltali_shuffle);
                break;
            case 26:
                imgPokemon.setImageResource(R.drawable.phyllali);
                txtPokemon.setText("Phyllali");
                txtNumero.setText("Numéro : 470");
                txtType.setText("Type : Plante");
                txtEspece.setText("Espèce : Verdoyant");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/phyllali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.phyllali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.phyllali_shuffle);
                break;
            case 27:
                imgPokemon.setImageResource(R.drawable.givrali);
                txtPokemon.setText("Givrali");
                txtNumero.setText("Numéro : 471");
                txtType.setText("Type : Glace");
                txtEspece.setText("Espèce : Poudreuse");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/givrali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.givrali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.givrali_shuffle);
                break;
            case 28:
                imgPokemon.setImageResource(R.drawable.noctali);
                txtPokemon.setText("Noctali");
                txtNumero.setText("Numéro : 197");
                txtType.setText("Type : Ténèbres");
                txtEspece.setText("Espèce : Lune");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/noctali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.noctali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.noctali_shuffle);
                break;
            case 29:
                imgPokemon.setImageResource(R.drawable.mentali);
                txtPokemon.setText("Mentali");
                txtNumero.setText("Numéro : 196");
                txtType.setText("Type : Psy");
                txtEspece.setText("Espèce : Soleil");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/mentali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.mentali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.mentali_shuffle);
                break;
            case 30:
                imgPokemon.setImageResource(R.drawable.nymphali);
                txtPokemon.setText("Nymphali");
                txtNumero.setText("Numéro : 700");
                txtType.setText("Type : Fée");
                txtEspece.setText("Espèce : Attachant");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/nymphali";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.nymphali);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.nymphali_shuffle);
                break;
            case 31:
                imgPokemon.setImageResource(R.drawable.deoxys1);
                txtPokemon.setText("Deoxys (Normal)");
                txtNumero.setText("Numéro : 386");
                txtType.setText("Type : Psy");
                txtEspece.setText("Espèce : ADN");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/deoxys";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.deoxys);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_deoxys1);
                break;
            case 32:
                imgPokemon.setImageResource(R.drawable.deoxys2);
                txtPokemon.setText("Deoxys (Défense)");
                txtNumero.setText("Numéro : 386");
                txtType.setText("Type : Psy");
                txtEspece.setText("Espèce : ADN");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/deoxys-defense";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.deoxys);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_deoxys2);
                break;
            case 33:
                imgPokemon.setImageResource(R.drawable.deoxys3);
                txtPokemon.setText("Deoxys (Vitesse)");
                txtNumero.setText("Numéro : 386");
                txtType.setText("Type : Psy");
                txtEspece.setText("Espèce : ADN");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/deoxys-vitesse";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.deoxys);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_deoxys3);
                break;
            case 34:
                imgPokemon.setImageResource(R.drawable.deoxys4);
                txtPokemon.setText("Deoxys (Attaque)");
                txtNumero.setText("Numéro : 386");
                txtType.setText("Type : Psy");
                txtEspece.setText("Espèce : ADN");
                lienPokemon = "https://www.pokebip.com/pokedex/pokemon/deoxys-attaque";
                bruitage = MediaPlayer.create(CopainActivity.this, R.raw.deoxys);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_deoxys4);
                break;
        }

        if (MainActivity.getEffetSonore()) {
            bruitage.start();
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_copain, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");
        retourPage();
    }

    public void goPokedex(MenuItem item) {
        MainActivity.effetPopup();
        Log.i("PROJET LOG", "open pokedex");
        openWebPage(lienPokemon);

    }

    public void goBack(MenuItem item) {
        MainActivity.effetPopup();
        Log.i("PROJET LOG", "go back");
        retourPage();
    }

    @SuppressLint("QueryPermissionsNeeded")
    public void openWebPage(String url) {
        Log.i("PROJET WEB PAGE", "Try to open Web Page url : " + url);

        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(intent);
    }

    public void retourPage() {
        switch(MainActivity.getPageCopain()) {
            case 1:
                startActivity(new Intent(this, DresseurActivity.class));
                break;
            case 4:
                startActivity(new Intent(this, DeoxysActivity.class));
                break;
            default:
                startActivity(new Intent(this, CaptureActivity.class));
                break;
        }
        finish();
    }


}