package com.example.info0306_projet_antoine_duval.page1;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.page2.dresseur.CopainActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class CaptureActivity extends AppCompatActivity {
    private int nombreAleatoire;
    private int ancienNombre;
    private int sauvegarde;

    private static boolean started = false;

    private static MediaPlayer bruitage;
    private static MediaPlayer capture;

    private TextView txtRetourCapture;
    private TextView txtRelacher;
    private ImageView imgEquipe;
    private TextView nomEquipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_capture);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        Log.i("PROJET", "Capture");
        capture = MediaPlayer.create(CaptureActivity.this,R.raw.capture);

        nombreAleatoire = 0;
        lancementPokemon();

        txtRetourCapture = findViewById(R.id.textRetour);
        txtRetourCapture.setOnClickListener(v -> {
            goMain(txtRetourCapture);
            MainActivity.effetPopup();
        });

        txtRelacher = findViewById(R.id.textRelacher);
        if (MainActivity.getSavePokemon() != 0){
            txtRelacher.setVisibility(View.VISIBLE);
        } else {
            txtRelacher.setVisibility(View.INVISIBLE);
        }
        txtRelacher.setOnClickListener(v -> relacher(txtRelacher));

    }

    public void openActivityCopain(int page) {
        MainActivity.setPageCopain(page);

        if (MainActivity.getSavePokemon() != 0) {
            MainActivity.effetPopup();
            Log.i("PROJET", "Page copain validée");

            startActivity(new Intent(this, CopainActivity.class));
            finish();
        } else {
            if (MainActivity.getPageCopain() == 3) {
                MainActivity.effetPopup();
                Log.i("PROJET", "Page copain validée");

                startActivity(new Intent(this, CopainActivity.class));
                finish();
            }
            else {
                Log.i("PROJET", "Page copain refusée");
                Toast.makeText(this, "Vous n'avez pas de pokémon", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void lancementPokemon() {
        sauvegarde = MainActivity.getSavePokemon();
        imgEquipe = findViewById(R.id.imageEquipe);
        imgEquipe.setOnClickListener(v -> openActivityCopain(2));
        nomEquipe = findViewById(R.id.textEquipe);

        switch (sauvegarde) {
            case 0:
                imgEquipe.setImageResource(R.drawable.pokeball);
                nomEquipe.setText("Vous n'avez pas de pokemon");
                break;
            case 3:
                imgEquipe.setImageResource(R.drawable.pikachu);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#FFD93E'>Pikachu</font>"));
                break;
            case 6:
                imgEquipe.setImageResource(R.drawable.evoli);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#BF8C59'>Evoli</font>"));
                break;
            case 9:
                imgEquipe.setImageResource(R.drawable.blizzi);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#81AB9D'>Blizzi</font>"));
                break;
            case 11:
                imgEquipe.setImageResource(R.drawable.carchacrok);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#5776DB'>Carchacrok</font>"));
                break;
            case 13:
                imgEquipe.setImageResource(R.drawable.mega_tyrannocif);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un<br><font color='#7B9142'>Mega - Tyranocif</font>"));
                break;
            case 14:
                imgEquipe.setImageResource(R.drawable.groudon);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#DB2F11'>Groudon</font>"));
                break;
            case 15:
                imgEquipe.setImageResource(R.drawable.kyogre);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#0079C4'>Kyogre</font>"));
                break;
            case 16:
                imgEquipe.setImageResource(R.drawable.rayquaza);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#198747'>Rayquaza</font>"));
                break;
            case 17:
                imgEquipe.setImageResource(R.drawable.evoli_shiny);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#D4D29E'>Evoli (shiny)</font>"));
                break;
            case 19:
                imgEquipe.setImageResource(R.drawable.salameche);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#F0907E'>Salamèche</font>"));
                break;
            case 21:
                imgEquipe.setImageResource(R.drawable.etourmi);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#A19FA3'>Etourmi</font>"));
                break;
            case 22:
                imgEquipe.setImageResource(R.drawable.metamorph);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#C399D8'>Métamorph</font>"));
                break;
            case 23:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#77CBDF'>Aquali</font>"));
                imgEquipe.setImageResource(R.drawable.aquali);
                break;
            case 24:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#E87D4A'>Pyroli</font>"));
                imgEquipe.setImageResource(R.drawable.pyroli);
                break;
            case 25:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#F5D16E'>Voltali</font>"));
                imgEquipe.setImageResource(R.drawable.voltali);
                break;
            case 26:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#7CC79E'>Phyllali</font>"));
                imgEquipe.setImageResource(R.drawable.phyllali);
                break;
            case 27:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#B6E1DE'>Givrali</font>"));
                imgEquipe.setImageResource(R.drawable.givrali);
                break;
            case 28:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#ECC876'>Noctali</font>"));
                imgEquipe.setImageResource(R.drawable.noctali);
                break;
            case 29:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#DABAD2'>Mentali</font>"));
                imgEquipe.setImageResource(R.drawable.mentali);
                break;
            case 30:
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#EF9AAD'>Nymphali</font>"));
                imgEquipe.setImageResource(R.drawable.nymphali);
                break;
        }
        generatePokemon(imgEquipe);

    }

    public void goMain(View v)
    {
        MainActivity.setSavePokemon(sauvegarde);
        started = false;
        Log.d("PROJET", "retour main");
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void autrePokemon(View v) {
        MainActivity.setPokemonSauvage(0);
        generatePokemon(v);
    }

    @SuppressLint("SetTextI18n")
    public void generatePokemon(View v)
    {
        ImageView imgPokemon = findViewById(R.id.imagePokemon);
        imgPokemon.setOnClickListener(f -> {
            openActivityCopain(3);
        });
        Button boutonC = findViewById(R.id.buttonCapture);
        TextView nomPokemon = findViewById(R.id.textCapture);

        do {
            if (MainActivity.getPokemonSauvage() == 0) {
                nombreAleatoire = (int) (Math.random() * (22) + 1);
            } else {
                nombreAleatoire = MainActivity.getPokemonSauvage();
            }
            switch(nombreAleatoire) {
                case 1:
                case 2:
                case 3:
                    nombreAleatoire = 3;
                    imgPokemon.setImageResource(R.drawable.pikachu);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.pikachu));
                    nomPokemon.setText("Pikachu");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.pikachu);
                    //Toast.makeText(this, "Pikachu apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                case 5:
                case 6:
                    nombreAleatoire = 6;
                    imgPokemon.setImageResource(R.drawable.evoli);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.evoli));
                    nomPokemon.setText("Evoli");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.evoli);
                    //Toast.makeText(this, "Evoli apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 7:
                case 8:
                case 9:
                    nombreAleatoire = 9;
                    imgPokemon.setImageResource(R.drawable.blizzi);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.blizzi));
                    nomPokemon.setText("Blizzi");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.blizzi);
                    //Toast.makeText(this, "Blizzi apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 10:
                case 11:
                    nombreAleatoire = 11;
                    imgPokemon.setImageResource(R.drawable.carchacrok);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.carchacrok));
                    nomPokemon.setText("Carchacrok");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.carchacrok);
                    //Toast.makeText(this, "Carchacrok apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 12:
                case 13:
                    nombreAleatoire = 13;
                    imgPokemon.setImageResource(R.drawable.mega_tyrannocif);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.tyrannocif));
                    nomPokemon.setText("Mega-Tyrnnocif");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.tyrannocif);
                    //Toast.makeText(this, "Mega-Tyranocif apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 14:
                    imgPokemon.setImageResource(R.drawable.groudon);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.groudon));
                    nomPokemon.setText("Groudon");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.groudon);
                    //Toast.makeText(this, "Groudon apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 15:
                    imgPokemon.setImageResource(R.drawable.kyogre);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.kyogre));
                    nomPokemon.setText("Kyogre");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.kyogre);
                    //Toast.makeText(this, "Groudon apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 16:
                    imgPokemon.setImageResource(R.drawable.rayquaza);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.rayquaza));
                    nomPokemon.setText("Rayquaza");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.rayquaza);
                    //Toast.makeText(this, "Rayquaza apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 17:
                    imgPokemon.setImageResource(R.drawable.evoli_shiny);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.evoli_shiny));
                    nomPokemon.setText("Evoli (shiny)");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.evoli);
                    //Toast.makeText(this, "Evoli (shiny) apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 18:
                case 19:
                    nombreAleatoire = 19;
                    imgPokemon.setImageResource(R.drawable.salameche);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.salameche));
                    nomPokemon.setText("Salamèche");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.salameche);
                    //Toast.makeText(this, "Salamèche apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 20:
                case 21:
                    nombreAleatoire = 21;
                    imgPokemon.setImageResource(R.drawable.etourmi);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.etourmi));
                    nomPokemon.setText("Etourmi");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.etourmi);
                    //Toast.makeText(this, "Etourmi apparaît", Toast.LENGTH_SHORT).show();
                    break;
                case 22:
                    imgPokemon.setImageResource(R.drawable.metamorph);
                    boutonC.setBackgroundColor(getResources().getColor(R.color.metamorph));
                    nomPokemon.setText("Metamorph");
                    bruitage = MediaPlayer.create(CaptureActivity.this,R.raw.metamorph);
                    //Toast.makeText(this, "Métamorph apparaît", Toast.LENGTH_SHORT).show();
                    break;
            }
            Log.d("PROJET", ""+nombreAleatoire+" - "+ancienNombre);
        } while (nombreAleatoire == ancienNombre);
        /*Pour avoir vraiment un autre pokemon quand on clique sur autre
        et pas juste un autre nombre aleatoire qui pourrait donner le meme affichage*/

        ancienNombre = nombreAleatoire;
        MainActivity.setPokemonSauvage(ancienNombre);

        if (started) {
            bruitageCapture();
        } else {
            if(MainActivity.getEffetSonore()) {
                bruitage.start();
            }
        } //MainActivity.theme.pause();
    }

    @SuppressLint("SetTextI18n")
    public void capturer(View v)
    {
        imgEquipe = findViewById(R.id.imageEquipe);
        nomEquipe = findViewById(R.id.textEquipe);
        switch(ancienNombre) {
            case 1:
            case 2:
            case 3:
                imgEquipe.setImageResource(R.drawable.pikachu);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#FFD93E'>Pikachu</font>"));
                //Toast.makeText(this, "Pikachu capturé", Toast.LENGTH_SHORT).show();
                break;
            case 4:
            case 5:
            case 6:
                imgEquipe.setImageResource(R.drawable.evoli);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#BF8C59'>Evoli</font>"));
                //Toast.makeText(this, "Evoli capturé", Toast.LENGTH_SHORT).show();
                break;
            case 7:
            case 8:
            case 9:
                imgEquipe.setImageResource(R.drawable.blizzi);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#81AB9D'>Blizzi</font>"));
                //Toast.makeText(this, "Blizzi capturé", Toast.LENGTH_SHORT).show();
                break;
            case 10:
            case 11:
                imgEquipe.setImageResource(R.drawable.carchacrok);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#5776DB'>Carchacrok</font>"));
                //Toast.makeText(this, "Carchacrok capturé", Toast.LENGTH_SHORT).show();
                break;
            case 12:
            case 13:
                imgEquipe.setImageResource(R.drawable.mega_tyrannocif);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un<br><font color='#7B9142'>Mega - Tyrnnocif</font>"));
                //Toast.makeText(this, "Mega-tyranocif capturé", Toast.LENGTH_SHORT).show();
                break;
            case 14:
                imgEquipe.setImageResource(R.drawable.groudon);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#DB2F11'>Groudon</font>"));
                //Toast.makeText(this, "Groudon capturé", Toast.LENGTH_SHORT).show();
                break;
            case 15:
                imgEquipe.setImageResource(R.drawable.kyogre);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#0079C4'>Kyogre</font>"));
                //Toast.makeText(this, "Kyogre capturé", Toast.LENGTH_SHORT).show();
                break;
            case 16:
                imgEquipe.setImageResource(R.drawable.rayquaza);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#198747'>Rayquaza</font>"));
                //Toast.makeText(this, "Rayquaza capturé", Toast.LENGTH_SHORT).show();
                break;
            case 17:
                imgEquipe.setImageResource(R.drawable.evoli_shiny);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#D4D29E'>Evoli (shiny)</font>"));
                //Toast.makeText(this, "Evoli (shiny) capturé", Toast.LENGTH_SHORT).show();
                break;
            case 18:
            case 19:
                imgEquipe.setImageResource(R.drawable.salameche);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#F0907E'>Salamèche</font>"));
                //Toast.makeText(this, "Salamèche capturé", Toast.LENGTH_SHORT).show();
                break;
            case 20:
            case 21:
                imgEquipe.setImageResource(R.drawable.etourmi);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#A19FA3'>Etourmi</font>"));
                //Toast.makeText(this, "Etourmi capturé", Toast.LENGTH_SHORT).show();
                break;
            case 22:
                imgEquipe.setImageResource(R.drawable.metamorph);
                nomEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#C399D8'>Métamorph</font>"));
                //Toast.makeText(this, "Métamorph capturé", Toast.LENGTH_SHORT).show();
                break;
        }
        sauvegarde = ancienNombre;
        MainActivity.setSavePokemon(sauvegarde);
        MainActivity.setPokemonSauvage(0);
        started = true;
        generatePokemon(nomEquipe);
        txtRelacher.setVisibility(View.VISIBLE);
        if(MainActivity.getEffetSonore()) {
            capture.start();
        }
    }

    @SuppressLint("SetTextI18n")
    public void relacher(View v)
    {
        if(sauvegarde != 0) {
            MainActivity.effetPopup();
            imgEquipe.setImageResource(R.drawable.pokeball);
            nomEquipe.setText("Vous n'avez pas de pokemon");
            txtRelacher.setVisibility(View.INVISIBLE);
            //Toast.makeText(this, "Pokemon relâché", Toast.LENGTH_SHORT).show();
            sauvegarde = 0;
            MainActivity.setSavePokemon(sauvegarde);
        } else {
            Toast.makeText(this, "Equipe déjà vide", Toast.LENGTH_SHORT).show();
        }
    }

    public void bruitageCapture() {
        TimerTask task = new TimerTask() {
            public void run() {
                if(MainActivity.getEffetSonore()) {
                    bruitage.start();
                }
            }
        };
        Timer timer = new Timer("timer");
        timer.schedule(task, 2800);
        started = false;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");


        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}