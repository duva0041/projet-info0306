package com.example.info0306_projet_antoine_duval.page3.contact_activity.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

@SuppressLint("ViewConstructor")
public class ItemList extends LinearLayout implements View.OnClickListener
{
    public ItemList(@NonNull Context context, Contact contact)
    {
        super(context);
        TextView contactView = new TextView(context);
        contactView.setText(contact.getName());
        contactView.setOnClickListener(new myOnclickListener(this, contactView));
        addView(contactView);
        setOrientation(LinearLayout.HORIZONTAL);
        Log.i("okd", "itemlist created");
    }

    @Override
    public void onClick(View view)
    {
        super.callOnClick();
        super.performClick();
        Log.i("okd", "okd");
    }

    static class myOnclickListener implements View.OnClickListener
    {
        TextView textView;
        ItemList parentToCall;
        public myOnclickListener(ItemList parentToCall, TextView textView)
        {
            this.textView = textView;
            this.parentToCall = parentToCall;
        }

        public void onClick(View v)
        {
        }
    }
}