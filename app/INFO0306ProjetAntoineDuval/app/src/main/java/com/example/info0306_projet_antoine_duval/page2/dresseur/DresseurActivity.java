package com.example.info0306_projet_antoine_duval.page2.dresseur;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.SecondActivity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class DresseurActivity extends AppCompatActivity {
    private TextView texteEquipe;
    private ImageView imageDresseur;
    private ImageView imageEquipe;
    private static MediaPlayer bruitage;
    private LocationManager locationManager = null;
    private String fournisseur;
    private boolean startedGps = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_dresseur);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        //Pour l'activité copain
        MainActivity.setPageCopain(1);

        TextView texteDresseur = findViewById(R.id.textPseudoDresseur);
        texteDresseur.setText(MainActivity.getPseudoDresseur());

        imageDresseur = findViewById(R.id.imgDresseur);
        imageEquipe = findViewById(R.id.imgPokemon);
        imageEquipe.setOnClickListener(v -> openActivityCopain());

        ImageView imgRetour = findViewById(R.id.imgRetour);
        imgRetour.setOnClickListener(v -> openActivityTwo(imgRetour));

        ImageView imgEvoli = findViewById(R.id.imgEvoli);
        imgEvoli.setOnClickListener(v -> openActivityEvoli());

        ImageView imgPokedex = findViewById(R.id.imgPokedex);
        imgPokedex.setOnClickListener(v -> openWebPage("https://www.pokemon.com/fr/pokedex/"));

        ImageView imgCombat = findViewById(R.id.imgCombat);
        imgCombat.setOnClickListener(v -> openWebPage("https://play.pokemonshowdown.com/"));

        ImageView imgNews = findViewById(R.id.imgNews);
        imgNews.setOnClickListener(v -> openWebPage("https://www.pokekalos.fr/"));

        texteEquipe = findViewById(R.id.textPokemonCapture);
        pokemonCapture();
        changeDresseur();
        verifGps();
    }

    public void openActivityCopain() {
        if (MainActivity.getSavePokemon() != 0) {
            MainActivity.effetPopup();
            Log.i("PROJET", "Page copain validée");

            startActivity(new Intent(this, CopainActivity.class));
            finish();
        } else {
            Log.i("PROJET", "Page copain refusée");
            Toast.makeText(this, "Vous n'avez pas de pokémon", Toast.LENGTH_SHORT).show();
        }
    }

    public void openActivityTwo(View img) {
        MainActivity.effetPopup();
        Log.i("PROJET", "Page 2");
        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    public void openActivityEvoli() {
        MainActivity.effetPopup();
        Log.i("PROJET", "Activité évoli");
        startActivity(new Intent(this, EvoliActivity.class));
        finish();
    }

    @SuppressLint("QueryPermissionsNeeded")
    public void openWebPage(String url) {
        MainActivity.effetPopup();
        Log.i("PROJET WEB PAGE", "Try to open Web Page url : " + url);

        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    public void pokemonCapture() {
        switch (MainActivity.getSavePokemon()) {
            case 0:
                texteEquipe.setText("Vous n'avez pas capturé de pokemon.");
                imageEquipe.setImageResource(R.drawable.pokeball);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.bip);
                break;
            case 3:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#FFD93E'>Pikachu</font>"));
                imageEquipe.setImageResource(R.drawable.pikachu);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.pikachu);
                break;
            case 6:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#BF8C59'>Evoli</font>"));
                imageEquipe.setImageResource(R.drawable.evoli);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.evoli);
                break;
            case 9:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#81AB9D'>Blizzi</font>"));
                imageEquipe.setImageResource(R.drawable.blizzi);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.bip);
                break;
            case 11:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#5776DB'>Carchacrok</font>"));
                imageEquipe.setImageResource(R.drawable.carchacrok);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.carchacrok);
                break;
            case 13:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <br><font color='#7B9142'>Mega - Tyranocif</font>"));
                imageEquipe.setImageResource(R.drawable.mega_tyrannocif);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.tyrannocif);
                break;
            case 14:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#DB2F11'>Groudon</font>"));
                imageEquipe.setImageResource(R.drawable.groudon);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.groudon);
                break;
            case 15:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#0079C4'>Kyogre</font>"));
                imageEquipe.setImageResource(R.drawable.kyogre);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.kyogre);
                break;
            case 16:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#198747'>Rayquaza</font>"));
                imageEquipe.setImageResource(R.drawable.rayquaza);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.rayquaza);
                break;
            case 17:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#D4D29E'>Evoli (shiny)</font>"));
                imageEquipe.setImageResource(R.drawable.evoli_shiny);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.evoli);
                break;
            case 19:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#F0907E'>Salamèche</font>"));
                imageEquipe.setImageResource(R.drawable.salameche);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.salameche);
                break;
            case 21:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#A19FA3'>Etourmi</font>"));
                imageEquipe.setImageResource(R.drawable.etourmi);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.etourmi);
                break;
            case 22:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#C399D8'>Métamorph</font>"));
                imageEquipe.setImageResource(R.drawable.metamorph);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.metamorph);
                break;
            case 23:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#77CBDF'>Aquali</font>"));
                imageEquipe.setImageResource(R.drawable.aquali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.aquali);
                break;
            case 24:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#E87D4A'>Pyroli</font>"));
                imageEquipe.setImageResource(R.drawable.pyroli);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.pyroli);
                break;
            case 25:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#F5D16E'>Voltali</font>"));
                imageEquipe.setImageResource(R.drawable.voltali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.voltali);
                break;
            case 26:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#7CC79E'>Phyllali</font>"));
                imageEquipe.setImageResource(R.drawable.phyllali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.phyllali);
                break;
            case 27:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#B6E1DE'>Givrali</font>"));
                imageEquipe.setImageResource(R.drawable.givrali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.givrali);
                break;
            case 28:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#ECC876'>Noctali</font>"));
                imageEquipe.setImageResource(R.drawable.noctali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.noctali);
                break;
            case 29:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#DABAD2'>Mentali</font>"));
                imageEquipe.setImageResource(R.drawable.mentali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.mentali);
                break;
            case 30:
                texteEquipe.setText(Html.fromHtml("Vous avez capturé un <font color='#EF9AAD'>Nymphali</font>"));
                imageEquipe.setImageResource(R.drawable.nymphali);
                bruitage = MediaPlayer.create(DresseurActivity.this, R.raw.nymphali);
                break;
        }
        if (MainActivity.getEffetSonore()) {
            bruitage.start();
        }
    }

    public void changeDresseur() {
        switch (MainActivity.getNumeroDresseur()) {
            case 0:
                imageDresseur.setImageResource(R.drawable.red);
                break;
            case 1:
                imageDresseur.setImageResource(R.drawable.blue);
                break;
            case 2:
                imageDresseur.setImageResource(R.drawable.gold);
                break;
            case 3:
                imageDresseur.setImageResource(R.drawable.cynthia);
                break;
            case 4:
                imageDresseur.setImageResource(R.drawable.peter);
                break;
            case 5:
                imageDresseur.setImageResource(R.drawable.pr_chen);
                break;
            case 6:
                imageDresseur.setImageResource(R.drawable.aurore);
                break;
            case 7:
                imageDresseur.setImageResource(R.drawable.james);
                break;
            case 8:
                imageDresseur.setImageResource(R.drawable.serena);
                break;
            case 9:
                imageDresseur.setImageResource(R.drawable.sacha);
                break;
            case 10:
                imageDresseur.setImageResource(R.drawable.tarak);
                break;
            case 11:
                imageDresseur.setImageResource(R.drawable.aurel);
                break;
        }
    }

    public void verifGps() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, 112);
            } else {
                geolocalisation();
            }
        } else {
            geolocalisation();
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 112) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                geolocalisation();
            } else {
                Toast.makeText(this, "Autorisation refusée", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void geolocalisation() {
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteres = new Criteria();
            criteres.setAccuracy(Criteria.ACCURACY_FINE);
            criteres.setAltitudeRequired(true);
            criteres.setBearingRequired(true);
            criteres.setSpeedRequired(true);
            criteres.setCostAllowed(true);
            criteres.setPowerRequirement(Criteria.POWER_HIGH);

            fournisseur = locationManager.getBestProvider(criteres, true);
            Log.d("GPS", "fournisseur : " + fournisseur);
        }

        if (fournisseur != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return; //Pour éviter une erreur meme si j'ai une fonction dédiée
            }
            Location localisation = locationManager.getLastKnownLocation(fournisseur);
            if (localisation != null) {
                Log.d("GPS", "localisation : " + localisation);
                @SuppressLint("DefaultLocale") String coordonnees = String.format("Latitude : %f - Longitude : %f\n", localisation.getLatitude(), localisation.getLongitude());
                Log.d("GPS", "coordonnees : " + coordonnees);
                @SuppressLint("DefaultLocale") String autres = String.format("Vitesse : %f - Altitude : %f - Cap : %f\n", localisation.getSpeed(), localisation.getAltitude(), localisation.getBearing());
                Log.d("GPS", autres);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date(localisation.getTime());
                Log.d("GPS", sdf.format(date));

                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> adresses = null;

                try {
                    adresses = geocoder.getFromLocation(localisation.getLatitude(), localisation.getLongitude(), 1);
                } catch (IOException ioException) {
                    Log.e("GPS", "erreur", ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    Log.e("GPS", "erreur " + coordonnees, illegalArgumentException);
                }

                if (adresses == null || adresses.size() == 0) {
                    Log.e("GPS", "erreur aucune adresse !");
                } else {
                    Address adresse = adresses.get(0);
                    ArrayList<String> addressFragments = new ArrayList<>();
                    for (int i = 0; i <= adresse.getMaxAddressLineIndex(); i++) {
                        addressFragments.add(adresse.getAddressLine(i));
                    }

                    TextView txtGPS = findViewById(R.id.textGPS);
                    txtGPS.setText(TextUtils.join(System.getProperty("line.separator"), addressFragments));
                    Log.d("GPS", TextUtils.join(System.getProperty("line.separator"), addressFragments));
                }
            } else {
                rechargementGPS();
            }
        } else {
            rechargementGPS();
        }
    }

    //Charge les données toutes les 2 secondes jusqu'à ce que l'utilisateur active le GPS pour ensuite afficher le texte
    public void rechargementGPS() {
        TimerTask task = new TimerTask() {
            public void run() {
                Log.d("GPS", "Timer gps");
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Criteria criteres = new Criteria();
                criteres.setAccuracy(Criteria.ACCURACY_FINE);
                criteres.setAltitudeRequired(true);
                criteres.setBearingRequired(true);
                criteres.setSpeedRequired(true);
                criteres.setCostAllowed(true);
                criteres.setPowerRequirement(Criteria.POWER_HIGH);
                fournisseur = locationManager.getBestProvider(criteres, true);
                Log.d("GPS", "fournisseur : " + fournisseur);

                geolocalisation();
            }
        };
        Timer timer = new Timer("timer gps");
        timer.schedule(task, 1200);
        if (startedGps) {
            Toast.makeText(this, "Activez votre localisation", Toast.LENGTH_SHORT).show();
            startedGps = false;
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

}