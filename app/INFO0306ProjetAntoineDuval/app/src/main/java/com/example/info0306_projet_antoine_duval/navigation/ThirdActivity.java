package com.example.info0306_projet_antoine_duval.navigation;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.info0306_projet_antoine_duval.page3.MagicarpeActivity;
import com.example.info0306_projet_antoine_duval.page3.parametres.ParametresActivity;
import com.example.info0306_projet_antoine_duval.R;

import java.util.Objects;

public class ThirdActivity extends AppCompatActivity {
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_third);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        Log.i("PROJET", "Create page 3");
        //Toast.makeText(this, "Page 3", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET", "Start");
        /* Toast.makeText(this, "Start", Toast.LENGTH_SHORT).show(); */
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET", "Resume");
        /* Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET", "Pause");
        /* Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();  */
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET", "Stop");
        /* Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET", "Restart");
        /* Toast.makeText(this, "Restart", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET", "Destroy");
        /* Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show(); */
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET", "BackPressed");
        /* Toast.makeText(this, "BackPressed", Toast.LENGTH_SHORT).show(); */


        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    public void goMain(View v)
    {
        Log.d("PROJET", "retour main");
        MainActivity.effetPopup();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void openActivityTwo(View button)
    {
        MainActivity.effetPopup();
        Log.i("PROJET", "Page 2");
        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    public void openContact(View button) {
        MainActivity.effetPopup();
        Log.i("PROJET", "open contact");
        verifAutorisation();
    }

    public void openParametres(View button) {
        Log.i("PROJET", "open parametres");
        startActivity(new Intent(this, ParametresActivity.class));
        finish();
    }

    public void openMagicarpe(View button) {
        MainActivity.effetPopup();
        Log.i("PROJET", "open magicarpe");
        startActivity(new Intent(this, MagicarpeActivity.class));
        finish();
    }

    public void goContact() {
        startActivity(new Intent(this, com.example.info0306_projet_antoine_duval.page3.contact_activity.activities.MainActivity.class));
        finish();
    }

    public void verifAutorisation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Autorisation contacts");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Veuillez autoriser l'accès aux contacts.");
                    builder.setOnDismissListener(dialog -> requestPermissions(new String[] {android.Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS));
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                goContact();
            }
        } else {
            goContact();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                goContact();
            } else {
                Toast.makeText(this, "Autorisation refusée", Toast.LENGTH_SHORT).show();
            }
        }
    }
}