package com.example.info0306_projet_antoine_duval;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.VideoView;

import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.page2.dresseur.DresseurActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class ChargementActivity extends AppCompatActivity {
    private boolean ready;
    private NotificationManagerCompat notificationManagerCompat;
    private NotificationCompat.Builder notification;
    private int niveauNotification;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargement);
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        notificationLancement();

        VideoView videoChargement = findViewById(R.id.videoChargement);
        ready = true;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.FrameLayout.LayoutParams params = (android.widget.FrameLayout.LayoutParams) videoChargement.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.gravity = Gravity.CENTER;
        videoChargement.setLayoutParams(params);

        videoChargement.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video_chargement));
        videoChargement.start();
        videoChargement.setOnClickListener(v -> goMain());

        TimerTask task = new TimerTask() {
            public void run() {  goMain(); }
        };
        Timer timer = new Timer("timer");
        timer.schedule(task, 7500);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void goMain()
    {
        if (ready) {
            MediaPlayer startSound = MediaPlayer.create(ChargementActivity.this,R.raw.start);
            startSound.start();
            Log.d("PROJET LOG", "retour main");
            startActivity(new Intent(this, MainActivity.class));
            finNotification();
            finish();
        }
        ready = false;
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void notificationLancement() {

        NotificationChannel channel1 = new NotificationChannel(
                "channel1",
                "Channel 1",
                NotificationManager.IMPORTANCE_HIGH
        );

        NotificationManager manager = this.getSystemService(NotificationManager.class);
        manager.createNotificationChannel(channel1);

        channel1.setDescription("This is channel 1");
        channel1.enableVibration(true);

        this.notificationManagerCompat = NotificationManagerCompat.from(this);
        sendOnChannel1();
    }

    public void sendOnChannel1()  {
        niveauNotification = 0;
        notification = new NotificationCompat.Builder(this, "channel1")
                .setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.capture))
                .setSmallIcon(R.drawable.pokedex)
                .setContentTitle("Chargement en cours ("+niveauNotification+"%).")
                .setContentText("Projet d'Antoine Duval (S3F3)")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setProgress(100, niveauNotification, false)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000);
                //.setSilent(true);
        notificationManagerCompat.notify(1, notification.build());
        addNotification();
    }

    public void addNotification() {
        TimerTask task = new TimerTask() {
            public void run() {
                if(niveauNotification<100 && ready) {
                    niveauNotification = niveauNotification + ((int) (Math.random() * 3) +1);
                    if (niveauNotification > 100) {
                        niveauNotification = 100;
                    }
                    notification
                            .setContentTitle("Chargement en cours ("+niveauNotification+"%).")
                            .setProgress(100, niveauNotification, false)
                            .setSilent(true);
                    notificationManagerCompat.notify(1, notification.build());
                    addNotification();
                } else {
                    finNotification();
                }
            }
        };
        Timer timer = new Timer("timer notification");
        timer.schedule(task, 130);
    }

    public void finNotification() {
        Intent resultIntent = new Intent(this, DresseurActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        }
        notification
                .setContentTitle("Chargement terminé")
                .setProgress(0, 0, false)
                .setSilent(false)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setSound(null)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000);
        notificationManagerCompat.notify(1, notification.build());
    }

}