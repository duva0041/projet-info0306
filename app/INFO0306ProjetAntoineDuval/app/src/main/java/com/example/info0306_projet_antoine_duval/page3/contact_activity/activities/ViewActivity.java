package com.example.info0306_projet_antoine_duval.page3.contact_activity.activities;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.ThirdActivity;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.data.Contact;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.fragments.ContactViewFragment;

import java.util.Objects;


public class ViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Profil d'un contact</font>"));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_contact_view);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#272727"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        setContentView(R.layout.activity_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


    }

    @Override
    public void onStart()
    {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        Contact contact = bundle.getParcelable("contact");
        ((ContactViewFragment) Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.ContactViewFragment))).setPhone(contact);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, com.example.info0306_projet_antoine_duval.page3.contact_activity.activities.MainActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact_view, menu);
        return true;
    }

    public void onClickMenu(MenuItem menuItem)
    {
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();
    }

    public void openContact(MenuItem m)
    {
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();
        Log.i("PROJET", "open contact");
        startActivity(new Intent(this, com.example.info0306_projet_antoine_duval.page3.contact_activity.activities.MainActivity.class));
        finish();
    }
}