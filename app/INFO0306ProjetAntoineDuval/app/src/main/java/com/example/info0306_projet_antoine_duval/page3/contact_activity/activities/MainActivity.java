package com.example.info0306_projet_antoine_duval.page3.contact_activity.activities;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.ThirdActivity;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.fragments.ContactListFragment;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.fragments.ContactViewFragment;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.interfaces.OnClickedItemContactListFragment;
import com.example.info0306_projet_antoine_duval.page3.contact_activity.data.ContactArrayAdapter;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements OnClickedItemContactListFragment
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Vos contacts</font>"));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_contact);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#272727"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        setContentView(R.layout.activity_contact_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


    }
    @Override
    public void onStart()
    {
        super.onStart();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction();
        ContactListFragment contactListFragment = (ContactListFragment) fragmentManager.findFragmentById(R.id.ContactListFragment);

        assert contactListFragment != null;
        contactListFragment.setOnItemClickListenerContactList(this);
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact, menu);
        return true;
    }

    public void openActivityThree(MenuItem m)
    {
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();
        Log.i("PROJET", "page 2");
        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Log.i("itemClicked", parent.getItemAtPosition(position).toString());
        com.example.info0306_projet_antoine_duval.navigation.MainActivity.effetPopup();

        ContactViewFragment contactViewFragment = (ContactViewFragment) getSupportFragmentManager().findFragmentById(R.id.ContactViewFragment);
        if(contactViewFragment != null)
        {
            contactViewFragment.setPhone(ContactArrayAdapter.getContactAt(parent, position));
        }
        else
        {
            Intent intent = new Intent(this, ViewActivity.class);
            intent.putExtra("contact", ContactArrayAdapter.getContactAt(parent, position));
            startActivity(intent);
            finish();
        }
    }
}