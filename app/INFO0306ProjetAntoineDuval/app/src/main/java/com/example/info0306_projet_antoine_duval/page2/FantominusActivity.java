package com.example.info0306_projet_antoine_duval.page2;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.SecondActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class FantominusActivity extends AppCompatActivity {
    private int niveauFantominus;
    private MediaPlayer cri;
    private ImageButton boutonPlus;
    private ImageButton boutonMoins;
    private ImageView imgFantominus;
    private TextView txtFantominus;
    private TextView txtRetour;
    private final String [] listFantominus = { "Fantominus", "Spectrum", "Ectoplasma", "M-Ectoplasma", "Ectoplasma Gx" };
    private ProgressBar barFantominus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_fantominus);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        niveauFantominus = 1;
        barFantominus    = findViewById(R.id.progressBarFantominus);
        imgFantominus    = findViewById(R.id.imageFantominus);
        boutonPlus       = findViewById(R.id.buttonPlus);
        boutonMoins      = findViewById(R.id.buttonMoins);
        txtFantominus    = findViewById(R.id.textNiveau);
        txtRetour        = findViewById(R.id.txtRetour);
        txtRetour.setOnClickListener(v -> {
            goMain(txtRetour);
            MainActivity.effetPopup();
        });

        changeImage();
        Log.i("PROJET", "Create page fantominus");
        //Toast.makeText(this, listFantominus[niveauFantominus-1], Toast.LENGTH_SHORT).show();
    }

    public void plusNiveau(View v) {
        if(niveauFantominus!=5) {
            niveauFantominus++;
            changeImage();
            barFantominus.setProgress(niveauFantominus);
            //Toast.makeText(this, listFantominus[niveauFantominus-1], Toast.LENGTH_SHORT).show();
            MainActivity.effetBip();
            if(niveauFantominus == 5) {
                boutonPlus.setBackgroundResource(R.drawable.bouton_plus_false);
            } else {
                boutonPlus.setBackgroundResource(R.drawable.bouton_plus_false);
                TimerTask task = new TimerTask() {
                    public void run() {
                        //Pour éviter que le bouton reste blanc si on spam-clique sur le bouton
                        if (niveauFantominus != 5) {
                            boutonPlus.setBackgroundResource(R.drawable.bouton_plus_true);
                        }
                    }
                };
                Timer timer = new Timer("timer bouton +");
                timer.schedule(task, 200);
                boutonMoins.setBackgroundResource(R.drawable.bouton_moins_true);
            }
        } else {
            Toast.makeText(this, "Evolution maximum", Toast.LENGTH_SHORT).show();
        }
    }

    public void moinsNiveau(View v) {
        if(niveauFantominus!=1) {
            niveauFantominus--;
            changeImage();
            barFantominus.setProgress(niveauFantominus);
            //Toast.makeText(this, listFantominus[niveauFantominus-1], Toast.LENGTH_SHORT).show();
            MainActivity.effetBip();
            if(niveauFantominus == 1) {
                boutonMoins.setBackgroundResource(R.drawable.bouton_moins_false);
            } else {
                boutonMoins.setBackgroundResource(R.drawable.bouton_moins_false);
                TimerTask task = new TimerTask() {
                    public void run() {
                        //Pour éviter que le bouton reste blanc si on spam-clique sur le bouton
                        if (niveauFantominus != 1) {
                            boutonMoins.setBackgroundResource(R.drawable.bouton_moins_true);
                        }
                    }
                };
                Timer timer = new Timer("timer bouton +");
                timer.schedule(task, 200);
                boutonPlus.setBackgroundResource(R.drawable.bouton_plus_true);
            }
        } else {
            Toast.makeText(this, "Forme de base", Toast.LENGTH_SHORT).show();
        }
    }

    public void changeImage() {
        switch(niveauFantominus) {
            case 1:
                imgFantominus.setImageResource(R.drawable.fantominus);
                cri = MediaPlayer.create(FantominusActivity.this, R.raw.fantominus);
                barFantominus.getProgressDrawable().setTint(getResources().getColor(R.color.fantominus));
                txtFantominus.setText(Html.fromHtml(listFantominus[niveauFantominus-1] + "(<font color='#B598B6'>"+niveauFantominus+"</font>/5)"));
                break;
            case 2:
                imgFantominus.setImageResource(R.drawable.spectrum);
                cri = MediaPlayer.create(FantominusActivity.this, R.raw.spectrum);
                barFantominus.getProgressDrawable().setTint(getResources().getColor(R.color.spectrum));
                txtFantominus.setText(Html.fromHtml(listFantominus[niveauFantominus-1] + "(<font color='#A199B0'>"+niveauFantominus+"</font>/5)"));
                break;
            case 3:
                imgFantominus.setImageResource(R.drawable.ectoplasma);
                cri = MediaPlayer.create(FantominusActivity.this, R.raw.ectoplasma);
                barFantominus.getProgressDrawable().setTint(getResources().getColor(R.color.ectoplasma));
                txtFantominus.setText(Html.fromHtml(listFantominus[niveauFantominus-1] + "(<font color='#EB9C8F'>"+niveauFantominus+"</font>/5)"));
                break;
            case 4:
                imgFantominus.setImageResource(R.drawable.mega_ectoplasma);
                cri = MediaPlayer.create(FantominusActivity.this, R.raw.mega_ectoplasma);
                barFantominus.getProgressDrawable().setTint(getResources().getColor(R.color.mega_ectoplasma));
                txtFantominus.setText(Html.fromHtml(listFantominus[niveauFantominus-1] + "(<font color='#D25887'>"+niveauFantominus+"</font>/5)"));
                break;
            case 5:
                imgFantominus.setImageResource(R.drawable.gigamax_ecotplasma);
                cri = MediaPlayer.create(FantominusActivity.this, R.raw.ectoplasma_gigamax);
                barFantominus.getProgressDrawable().setTint(getResources().getColor(R.color.ectoplasma_gigamax));
                txtFantominus.setText(Html.fromHtml(listFantominus[niveauFantominus-1] + "(<font color='#D81D45'>"+niveauFantominus+"</font>/5)"));
                break;
        }
        if(MainActivity.getEffetSonore()) {
            cri.start();
        }
    }

    public void goMain(View button)
    {
        Log.d("PROJET", "retour main");
        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

}