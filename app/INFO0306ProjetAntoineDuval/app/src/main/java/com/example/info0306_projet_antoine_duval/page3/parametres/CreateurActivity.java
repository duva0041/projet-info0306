package com.example.info0306_projet_antoine_duval.page3.parametres;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.ThirdActivity;

import java.util.Objects;

public class CreateurActivity extends AppCompatActivity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createur);
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        WebView viewInsta = findViewById(R.id.webViewInsta);
        viewInsta.loadUrl("https://www.instagram.com/p/B0I9uNui6bl/");
        viewInsta.getSettings().setJavaScriptEnabled(true);
        viewInsta.setWebViewClient(new MyBrowser());
        viewInsta.getSettings().setLoadsImagesAutomatically(true);
        viewInsta.getSettings().setJavaScriptEnabled(true);
        viewInsta.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        WebView viewYoutube = findViewById(R.id.webViewYoutube);
        viewYoutube.loadUrl("https://www.youtube.com/c/iRektPhotographie");
        viewYoutube.getSettings().setJavaScriptEnabled(true);
        viewYoutube.setWebViewClient(new MyBrowser());
        viewYoutube.getSettings().setLoadsImagesAutomatically(true);
        viewYoutube.getSettings().setJavaScriptEnabled(true);
        viewYoutube.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        ImageView imgRetour = findViewById(R.id.creaRetour);
        imgRetour.setOnClickListener(v -> goBack());

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, ParametresActivity.class));
        finish();
    }

    public void goBack() {
        MainActivity.effetPopup();
        Log.i("PROJET LOG", "Go back");

        startActivity(new Intent(this, ParametresActivity.class));
        finish();

    }

    private static class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}