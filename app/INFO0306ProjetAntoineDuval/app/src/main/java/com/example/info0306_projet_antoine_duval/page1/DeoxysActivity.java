package com.example.info0306_projet_antoine_duval.page1;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.page2.dresseur.CopainActivity;

import java.util.Objects;

public class DeoxysActivity extends AppCompatActivity {
    private SeekBar sDeoxys;
    private TextView tDeoxys;
    private int niveauDeoxys = 20;
    private ImageView imgDeoxys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_deoxys);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        Log.i("PROJET", "Page deoxys");
        Toast.makeText(this, "Deoxys", Toast.LENGTH_SHORT).show();

        TextView txtRetour = findViewById(R.id.titreDeoxys2);
        txtRetour.setOnClickListener(v -> {
            goMain();
            MainActivity.effetPopup();
        });

        imgDeoxys = findViewById(R.id.imageDeoxys);
        imgDeoxys.setOnClickListener(v -> openActivityCopain(4));

        MediaPlayer criDeoxys = MediaPlayer.create(DeoxysActivity.this,R.raw.deoxys);
        if(MainActivity.getEffetSonore()) {
            criDeoxys.start();
        }

        sDeoxys = findViewById(R.id.seekBarDeoxys);
        niveauDeoxys = MainActivity.getNiveauDeoxys();
        sDeoxys.setProgress(niveauDeoxys);
        tDeoxys = findViewById(R.id.niveauDeoxys);
        changeDeoxys();
        sDeoxys.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                niveauDeoxys = progress;
                changeDeoxys();
                Log.i("PROJET", "Progress changed seek bar deoxys");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                MainActivity.effetPopup();
                Log.i("PROJET", "Start tracking seek bar deoxys");
                //vide
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(MainActivity.getEffetSonore()) {
                    criDeoxys.start();
                    MainActivity.setNiveauDeoxys(niveauDeoxys);
                }
                Log.i("PROJET", "Seek Bar Deoxys : " + niveauDeoxys + "/" + sDeoxys.getMax());
            }
        });
    }

    public void goMain()
    {
        MainActivity.effetPopup();
        Log.d("PROJET", "retour main");
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    public void openActivityCopain(int page) {
        MainActivity.setPageCopain(page);

        MainActivity.effetPopup();
        Log.i("PROJET", "Page copain validée");

        startActivity(new Intent(this, CopainActivity.class));
        finish();
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    public void changeDeoxys()
    {
        if(niveauDeoxys<=25) {
            MainActivity.setNumeroDeoxys(31);
            imgDeoxys.setImageResource(R.drawable.deoxys1);
            sDeoxys.setThumb(getResources().getDrawable(R.drawable.ic_deoxys1));
            tDeoxys.setText(Html.fromHtml("Niveau de Deoxys : <font color='#47ACC5'>" + niveauDeoxys + "</font>/" + sDeoxys.getMax() + "<br><font color='#47ACC5'> Forme normale</font>"));
        } else {
            if(niveauDeoxys<=50) {
                MainActivity.setNumeroDeoxys(32);
                imgDeoxys.setImageResource(R.drawable.deoxys2);
                sDeoxys.setThumb(getResources().getDrawable(R.drawable.ic_deoxys2));
                tDeoxys.setText(Html.fromHtml("Niveau de Deoxys : <font color='#D56530'>" + niveauDeoxys + "</font>/" + sDeoxys.getMax() + "<br><font color='#D56530'> Forme défense</font>"));
            } else {
                if(niveauDeoxys<=75) {
                    MainActivity.setNumeroDeoxys(33);
                    imgDeoxys.setImageResource(R.drawable.deoxys3);
                    sDeoxys.setThumb(getResources().getDrawable(R.drawable.ic_deoxys3));
                    tDeoxys.setText(Html.fromHtml("Niveau de Deoxys : <font color='#807A72'>" + niveauDeoxys + "</font>/" + sDeoxys.getMax() + "<br><font color='#807A72'> Forme vitesse</font>"));
                } else {
                    MainActivity.setNumeroDeoxys(34);
                    imgDeoxys.setImageResource(R.drawable.deoxys4);
                    sDeoxys.setThumb(getResources().getDrawable(R.drawable.ic_deoxys4));
                    tDeoxys.setText(Html.fromHtml("Niveau de Deoxys : <font color='#DA70EF'>" + niveauDeoxys + "</font>/" + sDeoxys.getMax() + "<br><font color='#DA70EF'> Forme attaque</font>"));
                }
            }
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");


        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}