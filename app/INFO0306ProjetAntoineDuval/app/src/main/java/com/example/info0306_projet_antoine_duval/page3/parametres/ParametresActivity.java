package com.example.info0306_projet_antoine_duval.page3.parametres;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.ThirdActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class ParametresActivity extends AppCompatActivity {
    private final String[] listeItems = {"Red", "Blue", "Gold", "Cynthia", "Peter", "Pr Chen", "Aurore", "James", "Serena", "Sacha", "Tarak", "Aurel"};
    private ImageView imageDresseur;
    private SeekBar volumeSeekbar;
    private boolean surLaPage;

    private AudioManager audioManager = null;
    private TextView textVolume;
    private EditText editPseudo;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#272727'>Paramètres</font>"));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_parametre);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#FFFFFF"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        surLaPage = true;

        editPseudo = findViewById(R.id.editPseudo);
        editPseudo.setText(MainActivity.getPseudoDresseur());

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        textVolume = findViewById(R.id.textNiveauVolume);
        if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
            textVolume.setText("coupé");
        } else {
            textVolume.setText(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + "/15");
        }
        controlVolume();

        Log.i("PROJET LOG", "Create parametres");
        imageDresseur = findViewById(R.id.imageDresseur);
        changeImage(MainActivity.getNumeroDresseur());

        @SuppressLint("UseSwitchCompatOrMaterialCode")
        Switch switchMusique = findViewById(R.id.switchMusique);
        switchMusique.setChecked(MainActivity.getMusiqueActivee());
        switchMusique.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)  {
                MainActivity.theme.start();
                MainActivity.setMusiqueActivee(true);
                Log.i("PROJET LOG", "Musique : true");
                //Toast.makeText(ParametresActivity.this, "Musique start", Toast.LENGTH_SHORT).show();
            } else {
                MainActivity.setMusiqueActivee(false);
                MainActivity.theme.pause();
                Log.i("PROJET LOG", "Musique : false");
                //Toast.makeText(ParametresActivity.this, "Musique pause", Toast.LENGTH_SHORT).show();
            } MainActivity.effetPopup();
        });

        ToggleButton toggleButton = findViewById(R.id.toggleButton);
        toggleButton.setTextOn(Html.fromHtml("Effets sonores : <font color='#2B9100'>activés</font>"));
        toggleButton.setTextOff(Html.fromHtml("Effets sonores : <font color='#AD0000'>désactivés</font>"));
        toggleButton.setChecked(MainActivity.getEffetSonore());
        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked)  {
                MainActivity.setEffetSonore(true);
                Log.i("PROJET LOG", "Effets sonores : true");
            } else {
                MainActivity.setEffetSonore(false);
                Log.i("PROJET LOG", "Effets sonores : false");
            } MainActivity.effetPopup();
        });

        Spinner spinnerImages = findViewById(R.id.spinnerImages);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_list, listeItems);
        adapter.setDropDownViewResource(R.layout.spinner_list);
        spinnerImages.setAdapter(adapter);
        spinnerImages.setSelection(MainActivity.getNumeroDresseur());
        spinnerImages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.effetPopup();
                onItemSelectedHandler(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                MainActivity.effetPopup();
            }
        });
    }

    public void controlVolume() {
        volumeSeekbar = findViewById(R.id.seekBarVolume);
        volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        affichageVolume();
        volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekbar)
            {
                MainActivity.effetBip();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekbar)
            {
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekbar, int niveauVolume, boolean arg2)
            {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, niveauVolume, 0);
                if (niveauVolume == 0) {
                    textVolume.setText("coupé");
                }else {
                    textVolume.setText(niveauVolume + "/15");
                }
            }
        });
    }

    public void affichageVolume() {
        if (surLaPage) {
            TimerTask task = new TimerTask() {
                public void run() {
                    volumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                    affichageVolume();
                }
            };
            Timer timer = new Timer("timer affichage volume");
            timer.schedule(task, 200);
        }
    }

    public void openPhotoActivity(MenuItem m) {
        Log.d("PROJET", "activité photo");
        MainActivity.effetPopup();

        startActivity(new Intent(this, PhotoActivity.class));
        finish();
    }

    public void openVideoActivity(MenuItem m) {
        Log.d("PROJET", "activité video");
        MainActivity.effetPopup();

        startActivity(new Intent(this, VideoActivity.class));
        finish();
    }

    public void createurApp(MenuItem m) {
        Log.d("PROJET", "activité créateur");
        MainActivity.effetPopup();

        startActivity(new Intent(this, CreateurActivity.class));
        finish();
    }

    //Remet les shared preferences à l'état initial pour TOUTE l'application (meme le quizz)
    public void defautApp(MenuItem m){
        Log.d("PROJET", "Valeurs par défaut");
        MainActivity.effetPopup();

        MainActivity.setNumeroDresseur(2);
        MainActivity.setPseudoDresseur("Dresseur");
        MainActivity.setSavePokemon(0);

        MainActivity.setScore(0);
        MainActivity.setNumeroQuestion(1);

        MainActivity.setNumeroMusique(1);

        if (MainActivity.getMusiqueActivee()) {
            MainActivity.theme.pause();
            MainActivity.theme.release();
            changementMusique(imageDresseur);
        } else {
            MainActivity.theme.release();
            changementMusique(imageDresseur);
            MainActivity.theme.start();
        }

        MainActivity.setEffetSonore(true);
        MainActivity.setMusiqueActivee(true);

        startActivity(new Intent(this, ParametresActivity.class));
        finish();
    }


    private void onItemSelectedHandler(int position) {
        MainActivity.setNumeroDresseur(position);
        changeImage(position);
        //Toast.makeText(getApplicationContext(), "Image : " + listeItems[position] ,Toast.LENGTH_SHORT).show();
    }

    public void changementMusique(View button) {
        switch (MainActivity.getNumeroMusique()) {
            case 1:
                MainActivity.theme.release();
                MainActivity.theme = MediaPlayer.create(ParametresActivity.this,R.raw.theme_2);
                MainActivity.setNumeroMusique(2);
                break;
            case 2:
                MainActivity.theme.release();
                MainActivity.theme = MediaPlayer.create(ParametresActivity.this,R.raw.theme_3);
                MainActivity.setNumeroMusique(3);
                break;
            case 3:
                MainActivity.theme.release();
                MainActivity.theme = MediaPlayer.create(ParametresActivity.this,R.raw.theme_4);
                MainActivity.setNumeroMusique(4);
                break;
            case 4:
                MainActivity.theme.release();
                MainActivity.theme = MediaPlayer.create(ParametresActivity.this,R.raw.theme_5);
                MainActivity.setNumeroMusique(5);
                break;
            case 5:
                MainActivity.theme.release();
                MainActivity.theme = MediaPlayer.create(ParametresActivity.this,R.raw.theme_1);
                MainActivity.setNumeroMusique(1);
                break;
        }
        if (MainActivity.getMusiqueActivee()) {
            MainActivity.theme.start();
        }
    }

    public void validerText(View button) {
        MainActivity.effetPopup();
        ImageButton imgValider = findViewById(R.id.imageButtonValider);
        imgValider.setImageResource(R.drawable.valider_error);
        TimerTask task = new TimerTask() {
            public void run() {
                imgValider.setImageResource(R.drawable.valider);
            }
        };
        Timer timer = new Timer("timer bouton valider");
        timer.schedule(task, 200);


        String pseudo = editPseudo.getText().toString();
        if (!(pseudo.equals(""))) {
            if (!(pseudo.equals(MainActivity.getPseudoDresseur()))) {
                MainActivity.setPseudoDresseur(pseudo);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
                Toast.makeText(getApplicationContext(), "Nouveau pseudo",Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Pseudo identique",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Pas de pseudo",Toast.LENGTH_SHORT).show();
        }
    }

    public void changeImage(int numero) {
        switch(numero) {
            case 0:
                imageDresseur.setImageResource(R.drawable.red);
                break;
            case 1:
                imageDresseur.setImageResource(R.drawable.blue);
                break;
            case 2:
                imageDresseur.setImageResource(R.drawable.gold);
                break;
            case 3:
                imageDresseur.setImageResource(R.drawable.cynthia);
                break;
            case 4:
                imageDresseur.setImageResource(R.drawable.peter);
                break;
            case 5:
                imageDresseur.setImageResource(R.drawable.pr_chen);
                break;
            case 6:
                imageDresseur.setImageResource(R.drawable.aurore);
                break;
            case 7:
                imageDresseur.setImageResource(R.drawable.james);
                break;
            case 8:
                imageDresseur.setImageResource(R.drawable.serena);
                break;
            case 9:
                imageDresseur.setImageResource(R.drawable.sacha);
                break;
            case 10:
                imageDresseur.setImageResource(R.drawable.tarak);
                break;
            case 11:
                imageDresseur.setImageResource(R.drawable.aurel);
                break;
        }
    }

    public void openActivityThree(MenuItem m)
    {
        Log.d("PROJET", "page 3");
        MainActivity.effetPopup();
        MainActivity.setPseudoDresseur(editPseudo.getText().toString());
        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    public void closeApp(MenuItem m) {
        Log.d("PROJET", "close app");
        MainActivity.effetPopup();
        //startActivity(new Intent(this, ChargementActivity.class));
        finish();
        System.exit(0);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_parametre, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    public void onClickMenu(MenuItem menuItem)
    {
        MainActivity.effetPopup();
        Log.d("PROJET LOG", "item cliqué");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        surLaPage = true;
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        surLaPage = false;
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        surLaPage = false;
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");
        MainActivity.setPseudoDresseur(editPseudo.getText().toString());

        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

}