package com.example.info0306_projet_antoine_duval.page3.contact_activity.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactAddFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ContactAddFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText name;
    private EditText number;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactAddFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactAddFragment newInstance(String param1, String param2) {
        ContactAddFragment fragment = new ContactAddFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ContactAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onStart()
    {
        super.onStart();

        name = requireView().findViewById(R.id.addNameText);
        number = requireView().findViewById(R.id.addNumberText);

        Button button = requireView().findViewById(R.id.addSubmitButton);
        button.setOnClickListener(view -> {
            MainActivity.effetPopup();
            Intent intent = new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT);

            Uri uri = Uri.fromParts("tel", String.valueOf(number.getText()), null);

            intent.setData(uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);
            intent.putExtra(ContactsContract.Intents.Insert.NAME, String.valueOf(name.getText()));

            startActivity(intent);
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_add, container, false);
    }
}