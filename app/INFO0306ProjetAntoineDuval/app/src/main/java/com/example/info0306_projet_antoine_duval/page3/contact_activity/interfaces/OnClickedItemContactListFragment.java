package com.example.info0306_projet_antoine_duval.page3.contact_activity.interfaces;

import android.view.View;
import android.widget.AdapterView;

public interface OnClickedItemContactListFragment
{
    void onItemClick(AdapterView<?> parent, View view, int position, long id);
}
