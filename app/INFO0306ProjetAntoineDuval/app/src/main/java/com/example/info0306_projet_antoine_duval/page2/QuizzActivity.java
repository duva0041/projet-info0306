package com.example.info0306_projet_antoine_duval.page2;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.SecondActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class QuizzActivity extends AppCompatActivity {
    private MediaPlayer good;
    private MediaPlayer fail;
    private MediaPlayer clap;

    private int numeroQuestion;
    private int score;
    private int choix;

    private boolean aChoisi = false;
    private boolean timerEnCours = false;

    private Button boutonA;
    private Button boutonB;
    private Button boutonC;
    private Button boutonD;
    private Button boutonValider;

    private TextView enonceQuestion;
    private ImageView imageQuestion;

    private TextView txtProgressBarQuestion;
    private ProgressBar progressBarQuestion;
    private int tempsQuestion = 300;
    private boolean timerFini = false;
    private boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);
        started = true;

        good = MediaPlayer.create(QuizzActivity.this,R.raw.good);
        fail = MediaPlayer.create(QuizzActivity.this,R.raw.bad);
        clap = MediaPlayer.create(QuizzActivity.this,R.raw.clap);

        numeroQuestion = MainActivity.getNumeroQuestion();
        score = MainActivity.getScore();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#272727"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Question " + numeroQuestion + "/10 &nbsp &nbsp &nbsp Score :&nbsp " + score + "</font>"));
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        boutonA = findViewById(R.id.buttonA);
        boutonB = findViewById(R.id.buttonB);
        boutonC = findViewById(R.id.buttonC);
        boutonD = findViewById(R.id.buttonD);

        boutonValider = findViewById(R.id.buttonValider);
        enonceQuestion = findViewById(R.id.textQuestion);
        imageQuestion = findViewById(R.id.imageQuestion);

        txtProgressBarQuestion = findViewById(R.id.textProgressBar);
        progressBarQuestion = findViewById(R.id.progressBarQuestion);

        creationQuizz();
        actualisationTitre();
    }

    @SuppressLint("SetTextI18n")
    public void creationQuizz() {
        aChoisi = false;
        timerFini = false;
        tempsQuestion = 300;
        changementProgressBar();

        boutonValider.setBackgroundColor(getResources().getColor(R.color.gris));
        boutonValider.setText("Valider");

        switch (numeroQuestion) {
            case 1:
                enonceQuestion.setText("Quel est ce pokémon ?");
                imageQuestion.setImageResource(R.drawable.ectoplasma_flou);
                boutonA.setText("Fantominus");
                boutonB.setText("Deoxys");
                boutonC.setText("Dialga");
                boutonD.setText("Ectoplasma");  //bonne réponse
                break;
            case 2:
                enonceQuestion.setText("De quel type est Absol, le pokémon catastrophe ?");
                boutonA.setText("Feu");
                boutonB.setText("Eau");
                boutonC.setText("Ténèbres");  //bonne réponse
                boutonD.setText("Vol");
                break;
            case 3:
                enonceQuestion.setText("Quelle est l'evolution maximale d'Embrylex ?");
                boutonA.setText("Mega Tyrannocif");  //bonne réponse
                boutonB.setText("Tyrannocif");
                boutonC.setText("Giratina");
                boutonD.setText("Rayquaza");
                break;
            case 4:
                enonceQuestion.setText("De quelle région provient Regigigas ?");
                boutonA.setText("Unys");
                boutonB.setText("Sinnoh");  //bonne réponse
                boutonC.setText("Kanto");
                boutonD.setText("Hoenn");
                break;
            case 5:
                enonceQuestion.setText("Lequel n'est pas de type Feu ?");
                boutonA.setText("Dracaufeu");
                boutonB.setText("Sulfura");
                boutonC.setText("Kyogre");  //bonne réponse
                boutonD.setText("Heatran");
                break;
            case 6:
                enonceQuestion.setText("Lequel n'est pas un légendaire ?");
                boutonA.setText("Lucario");  //bonne réponse
                boutonB.setText("Mewto");
                boutonC.setText("Arceus");
                boutonD.setText("Lugia");
                break;
            case 7:
                enonceQuestion.setText("Combien de formes a Deoxys ?");
                boutonA.setText("3");
                boutonB.setText("Une seule");
                boutonC.setText("6");
                boutonD.setText("4");  //bonne réponse
                break;
            case 8:
                enonceQuestion.setText("Quel est ce Pokemon ?");
                boutonA.setText("Tyrannocif");
                boutonB.setText("Groudon");
                boutonC.setText("Carchacrok");  //bonne réponse
                boutonD.setText("Kyurem");
                break;
            case 9:
                enonceQuestion.setText("Qui est le dieu des pokemons ?");
                boutonA.setText("Arceus");  //bonne réponse
                boutonB.setText("Deoxys");
                boutonC.setText("Palkia");
                boutonD.setText("Pikachu");
                break;
            case 10:
                enonceQuestion.setText("Lequel de ces pokémons est le gardien du Lac Courage ?");
                boutonA.setText("Giratina");
                boutonB.setText("Regice");
                boutonC.setText("Dracolosse");
                boutonD.setText("Créfadet");  //bonne réponse
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    public void changementProgressBar() {
        TimerTask taskBar = new TimerTask() {
            @SuppressLint("SetTextI18n")
            public void run() {
                if (tempsQuestion >= 0 && !timerFini) {
                    progressBarQuestion.setProgress(tempsQuestion);
                    txtProgressBarQuestion.setText(tempsQuestion/10.0 + "s");
                    if (tempsQuestion >= 250) {
                        txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz1));
                    } else {
                        if (tempsQuestion >= 200) {
                            txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz15));
                        }else {
                            if (tempsQuestion >= 150) {
                                txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz2));
                            } else {
                                if (tempsQuestion >= 100) {
                                    txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz25));
                                } else {
                                    if (tempsQuestion >= 50) {
                                        txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz3));
                                    } else {
                                        txtProgressBarQuestion.setTextColor(getResources().getColor(R.color.quizz4));
                                    }
                                }
                            }
                        }
                    }
                    tempsQuestion--;
                    changementProgressBar();
                }
            }
        };

        if (tempsQuestion == 0) {
            aChoisi = true;
            timerFini = true;
            choix = 5;
            boutonA.setBackgroundColor(getResources().getColor(R.color.white));
            boutonB.setBackgroundColor(getResources().getColor(R.color.white));
            boutonC.setBackgroundColor(getResources().getColor(R.color.white));
            boutonD.setBackgroundColor(getResources().getColor(R.color.white));
            progressBarQuestion.setProgress(0);
            txtProgressBarQuestion.setText("fin");
            verificationQuestion(boutonValider);
        } else {
            if (!timerFini) {
                Timer timerBar = new Timer("timer question quizz");
                timerBar.schedule(taskBar, 100);
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void choixQuestion(View button) {
        if (!timerEnCours) {
            aChoisi = true;
            MainActivity.effetPopup();
            boutonValider.setBackgroundColor(getResources().getColor(R.color.white));
            switch (button.getId()) {
                case R.id.buttonA:
                    choix = 1;
                    boutonA.setBackgroundColor(getResources().getColor(R.color.bleu));
                    boutonB.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonC.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonD.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case R.id.buttonB:
                    choix = 2;
                    boutonA.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonB.setBackgroundColor(getResources().getColor(R.color.bleu));
                    boutonC.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonD.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case R.id.buttonC:
                    choix = 3;
                    boutonA.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonB.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonC.setBackgroundColor(getResources().getColor(R.color.bleu));
                    boutonD.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case R.id.buttonD:
                    choix = 4;
                    boutonA.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonB.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonC.setBackgroundColor(getResources().getColor(R.color.white));
                    boutonD.setBackgroundColor(getResources().getColor(R.color.bleu));
                    break;
            }
        }
    }

    public void effetFail() {
        if (MainActivity.getEffetSonore()) {
            fail.start();
        }
    }

    public void effetGood() {
        if (MainActivity.getEffetSonore()) {
            good.start();
        }
    }

    @SuppressLint("SetTextI18n")
    public void verificationQuestion(View button) {
        if (aChoisi) {
            timerFini = true;
            progressBarQuestion.setVisibility(View.INVISIBLE);
            txtProgressBarQuestion.setVisibility(View.INVISIBLE);

            MainActivity.effetPopup();
            boutonValider.setBackgroundColor(getResources().getColor(R.color.gris));
            boutonValider.setText("Chargement");
            switch (numeroQuestion) {
                case 1:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Ectoplasma pas Fantominus");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Ectoplasma pas Deoxys");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Ectoplasma pas Dialga");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, c'est Ectoplasma");
                            effetGood();
                            score++;
                            break;
                        case 5:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, c'est Ectoplasma");
                            effetFail();
                            break;
                    }
                    break;

                case 2:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Absol est de type Ténèbres, pas Feu");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Absol est de type Ténèbres, pas Eau");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Absol est de type Ténèbres");
                            effetGood();
                            score++;
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Absol est de type Ténèbres, pas Vol");
                            effetFail();
                            break;
                        case 5:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps,  Absol est de type Ténèbres");
                            effetFail();
                            break;
                    }
                    break;

                case 3:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Embrylex peut évoluer jusqu'en Mega Tyrannocif");
                            effetGood();
                            score++;
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Embrylex peut évoluer jusqu'en Mega Tyrannocif, pas seulement en Tyrannocif");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Embrylex peut évoluer jusqu'en Mega Tyrannocif, pas en Giratina");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Embrylex peut évoluer jusqu'en Mega Tyrannocif, pas en Rayquaza");
                            effetFail();
                            break;
                        case 5:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, Embrylex peut évoluer jusqu'en Mega Tyrannocif");
                            effetFail();
                            break;
                    }
                    break;

                case 4:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonB.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Regigigas provient de Sinnoh, pas d'Unys");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Regigigas provient de Sinnoh");
                            effetGood();
                            score++;
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonB.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Regigigas provient de Sinnoh, pas de Kanto");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonB.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Regigigas provient de Sinnoh, pas d'Hoenn");
                            effetFail();
                            break;
                        case 5:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, Regigigas provient de Sinnoh");
                            effetFail();
                            break;
                    }
                    break;

                case 5:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Dracaufeu est de type feu, mais pas Kyogre");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Sulfura est de type feu, mais pas Kyogre");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Kyogre est de type eau");
                            effetGood();
                            score++;
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Heatran est de type feu, mais pas Kyogre");
                            effetFail();
                            break;
                        case 5:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, Kyogre est de type eau");
                            effetFail();
                            break;
                    }
                    break;

                case 6:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Lucario n'est pas un légendaire");
                            effetGood();
                            score++;
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Mewto est un légendaire, mais pas Lucario");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Arceus est un légendaire, mais pas Lucario");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Lugia est un légendaire, mais pas Lucario");
                            effetFail();
                            break;
                        case 5:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, Lucario n'est pas un légendaire");
                            effetFail();
                            break;
                    }
                    break;

                case 7:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Deoxys a 4 formes, pas 3");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Deoxys a 4 formes, pas une seule");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, Deoxys a 4 formes, pas 6");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, Deoxys a 4 formes");
                            effetGood();
                            score++;
                            break;
                        case 5:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, Deoxys a 4 formes");
                            effetFail();
                            break;
                    }
                    break;

                case 8:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Carchacrok pas Tyrannocif");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Carchacrok pas Groudon");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, c'est Carchacrok");
                            effetGood();
                            score++;
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonC.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Carchacrok pas Kyurem");
                            effetFail();
                            break;
                        case 5:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, c'est Carchacrok");
                            effetFail();
                            break;
                    }
                    break;

                case 9:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, c'est Arceus le dieu des pokemons");
                            effetGood();
                            score++;
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Arceus le dieu des pokemons, pas Deoxys");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Arceus le dieu des pokemons, pas Palkia");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonA.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Arceus le dieu des pokemons, pas Pikachu");
                            effetFail();
                            break;
                        case 5:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, c'est Arceus le dieu des pokemons");
                            effetFail();
                            break;
                    }
                    break;

                case 10:
                    switch (choix){
                        case 1:
                            boutonA.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Créfadet le gardien du Lac Courage, pas Giratina");
                            effetFail();
                            break;
                        case 2:
                            boutonB.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Créfadet le gardien du Lac Courage, pas Regice");
                            effetFail();
                            break;
                        case 3:
                            boutonC.setBackgroundColor(getResources().getColor(R.color.rouge));
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Mauvaise réponse, c'est Créfadet le gardien du Lac Courage, pas Dracolosse");
                            effetFail();
                            break;
                        case 4:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.vert));
                            enonceQuestion.setText("Bonne réponse, c'est Créfadet le gardien du Lac Courage");
                            effetGood();
                            score++;
                            break;
                        case 5:
                            boutonD.setBackgroundColor(getResources().getColor(R.color.orange));
                            enonceQuestion.setText("Fin du temps, c'est Créfadet le gardien du Lac Courage");
                            effetFail();
                            break;
                    }
                    break;

            }
            aChoisi = false;
            timerEnCours = true;

            imageReponse();
            runOnUiThread(() -> Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Question " + numeroQuestion + "/10 &nbsp &nbsp &nbsp Score :&nbsp " + score + "</font>")));
            questionSuivante();
        }
    }

    public void imageReponse() {
        runOnUiThread(() -> {
            switch(numeroQuestion) {
                case 1:
                    imageQuestion.setImageResource(R.drawable.ectoplasma);
                    break;
                case 2:
                    imageQuestion.setImageResource(R.drawable.absol_tenebres);
                    break;
                case 3:
                    imageQuestion.setImageResource(R.drawable.mega_tyrannocif);
                    break;
                case 4:
                    imageQuestion.setImageResource(R.drawable.regigigas_2);
                    break;
                case 5:
                    imageQuestion.setImageResource(R.drawable.kyogre);
                    break;
                case 6:
                    imageQuestion.setImageResource(R.drawable.lucario);
                    break;
                case 7:
                    imageQuestion.setImageResource(R.drawable.deoxys_formes);
                    break;
                case 8:
                    imageQuestion.setImageResource(R.drawable.carchacrok);
                    break;
                case 9:
                    imageQuestion.setImageResource(R.drawable.arceus);
                    break;
                case 10:
                    imageQuestion.setImageResource(R.drawable.crefadet);
                    break;

            }
        });
    }

    public void questionSuivante() {
        if (numeroQuestion != 10) {
            MainActivity.setNumeroQuestion(numeroQuestion+1);
            MainActivity.setScore(score);
            TimerTask taskQuestion = new TimerTask() {
                public void run() {
                    numeroQuestion++;
                    timerFini = true;
                    creationQuizz();
                    actualisationTitre();
                    Log.i("PROJET LOG QUIZZ", "Question suivante : "+numeroQuestion);
                }
            };
            Timer timerQuestion = new Timer("timer quizz");
            timerQuestion.schedule(taskQuestion, 3000);
        } else {
            TimerTask taskFin = new TimerTask() {
                public void run() {
                    finQuizz();
                }
            };
            Timer timerFin = new Timer("timer quizz");
            timerFin.schedule(taskFin, 3000);
        }
    }

    public void effetClap(){
        if(MainActivity.getEffetSonore()) {
            clap.start();
        }
    }

    @SuppressLint("SetTextI18n")
    public void finQuizz() {
        effetClap();
        runOnUiThread(() -> {
            Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Fin du quizz</font>"));
            imageQuestion.setImageResource(R.drawable.pr_platane);
            boutonA.setVisibility(View.INVISIBLE);
            boutonB.setVisibility(View.INVISIBLE);
            boutonC.setVisibility(View.INVISIBLE);
            boutonD.setVisibility(View.INVISIBLE);
            boutonValider.setVisibility(View.INVISIBLE);
            progressBarQuestion.setVisibility(View.INVISIBLE);
            txtProgressBarQuestion.setVisibility(View.INVISIBLE);
        });
        switch (score) {
            case 0:
                enonceQuestion.setText("Vous n'avez obtenu aucune bonne réponse sur les 10 questions...");
                break;
            case 1:
                enonceQuestion.setText("Vous avez obtenu une seule bonne réponse sur les 10 questions...");
                break;
            case 10:
                enonceQuestion.setText("Vous avez eu bon aux 10 questions ! C'est grandioooose");
                break;
            default:
                enonceQuestion.setText("Vous avez obtenu " + score + " bonnes réponses sur 10 questions.");
                break;
        }

        numeroQuestion = 1;
        MainActivity.setNumeroQuestion(numeroQuestion);

        score = 0;
        MainActivity.setScore(score);
    }

    //Obliger de passer par cette méthode pour modifer des éléments sans faire crasher l'application même si cela ne m'arrange pas
    @SuppressLint("SetTextI18n")
    public void actualisationTitre() {
        runOnUiThread(() -> {
            timerEnCours = false;
            boutonA.setBackgroundColor(getResources().getColor(R.color.white));
            boutonB.setBackgroundColor(getResources().getColor(R.color.white));
            boutonC.setBackgroundColor(getResources().getColor(R.color.white));
            boutonD.setBackgroundColor(getResources().getColor(R.color.white));

            progressBarQuestion.setVisibility(View.VISIBLE);
            txtProgressBarQuestion.setVisibility(View.VISIBLE);
            tempsQuestion = 300;
            progressBarQuestion.setProgress(tempsQuestion);
            txtProgressBarQuestion.setText(tempsQuestion/10.0 + "s");

            Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFFFFF'>Question " + numeroQuestion + "/10 &nbsp &nbsp &nbsp Score :&nbsp " + score + "</font>"));
            switch(numeroQuestion) {
                case 2:
                    imageQuestion.setImageResource(R.drawable.absol);
                    break;
                case 3:
                    imageQuestion.setImageResource(R.drawable.embrylex);
                    break;
                case 4:
                    imageQuestion.setImageResource(R.drawable.regigigas);
                    break;
                case 5:
                    imageQuestion.setImageResource(R.drawable.type_feu);
                    break;
                case 6:
                    imageQuestion.setImageResource(R.drawable.legendaire);
                    break;
                case 7:
                    imageQuestion.setImageResource(R.drawable.deoxys1);
                    break;
                case 8:
                    imageQuestion.setImageResource(R.drawable.carchacrok_flou);
                    break;
                case 9:
                    imageQuestion.setImageResource(R.drawable.dieu_pokemon);
                    break;
                case 10:
                    imageQuestion.setImageResource(R.drawable.lac_courage);
                    break;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume() {
        timerFini = false;
        //creationQuizz();
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }

    @Override
    protected void onPause() {
        timerFini = true;
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }

    @Override
    protected void onStop() {
        timerFini = true;
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }

    @Override
    protected void onRestart() {
        timerFini = false;
        if (started) {
            changementProgressBar();
        }
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }

    @Override
    protected void onDestroy() {
        timerFini = true;
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }

    @Override
    public void onBackPressed() {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_quizz, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    public void onClickMenu(MenuItem menuItem)
    {
        MainActivity.effetPopup();
    }

    public void openActivityTwo(MenuItem m)
    {
        MainActivity.effetPopup();
        Log.d("PROJET", "retour main");
        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    @SuppressLint("SetTextI18n")
    public void recommencerQuizz(MenuItem item) {
            aChoisi = false;
            timerEnCours = false;

            numeroQuestion = 1;
            MainActivity.setNumeroQuestion(numeroQuestion);

            score = 0;
            MainActivity.setScore(score);

            actualisationTitre();

            boutonA.setVisibility(View.VISIBLE);
            boutonB.setVisibility(View.VISIBLE);
            boutonC.setVisibility(View.VISIBLE);
            boutonD.setVisibility(View.VISIBLE);
            boutonValider.setVisibility(View.VISIBLE);

            progressBarQuestion.setVisibility(View.VISIBLE);
            txtProgressBarQuestion.setVisibility(View.VISIBLE);
            tempsQuestion = 300;
            progressBarQuestion.setProgress(tempsQuestion);
            txtProgressBarQuestion.setText(tempsQuestion/10.0 + "s");

            boutonA.setBackgroundColor(getResources().getColor(R.color.white));
            boutonB.setBackgroundColor(getResources().getColor(R.color.white));
            boutonC.setBackgroundColor(getResources().getColor(R.color.white));
            boutonD.setBackgroundColor(getResources().getColor(R.color.white));

            MainActivity.effetPopup();

            startActivity(new Intent(this, QuizzActivity.class));
            finish();
    }
}