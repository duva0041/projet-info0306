package com.example.info0306_projet_antoine_duval.page3;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;
import com.example.info0306_projet_antoine_duval.navigation.ThirdActivity;
import com.example.info0306_projet_antoine_duval.page3.parametres.ParametresActivity;

import java.util.Objects;

public class MagicarpeActivity extends AppCompatActivity implements View.OnDragListener, View.OnLongClickListener, SensorEventListener {
    private ImageView magicarpe;
    private ImageView lac;
    private ImageView leviator;
    private TextView txtMagicarpe;

    public static int x;
    public static int y;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private long lastUpdate;

    private static MediaPlayer victoire;
    private static MediaPlayer criMagicarpe;
    private static MediaPlayer shiny;
    private int nbAleatoire;
    private boolean lancement = true;

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("PROJET LOG", "Create");
        setContentView(R.layout.activity_magicarpe);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_magicarpe);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#272727"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#EB7F62'>Magicarpe</font>"));
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        victoire = MediaPlayer.create(MagicarpeActivity.this,R.raw.victoire);
        criMagicarpe = MediaPlayer.create(MagicarpeActivity.this,R.raw.magicarpe);
        shiny = MediaPlayer.create(MagicarpeActivity.this,R.raw.shiny);

        magicarpe = findViewById(R.id.imageMagicarpe);
        magicarpe.setTag("DRAGGABLE MAGICARPE");
        magicarpe.setOnLongClickListener(this);

        x=80;
        y=80;
        leviator = findViewById(R.id.imageLeviator);
        leviator.setY(y);
        leviator.setX(x);

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        lastUpdate = System.currentTimeMillis();

        txtMagicarpe = findViewById(R.id.textMagicarpe);
        txtMagicarpe.setText(Html.fromHtml("Il faut sauver " + "<font color='#EB7F62'>Magicarpe</font>" + " !! Attrape le et remet le dans l'eau."));

        nbAleatoire = ((int) (Math.random() * 5)) + 1;
        effetMagicarpe();
        shinyMagicarpe();

        lac = findViewById(R.id.imageLac);
        lac.setOnDragListener(this);
    }

    public void effetWin() {
        if (MainActivity.getEffetSonore()) {
            victoire.start();
        }
    }

    public void effetShiny() {
        if (lancement) {
            if(MainActivity.getEffetSonore()) {
                shiny.start();
            }
            txtMagicarpe.setText(Html.fromHtml("Il faut sauver " + "<font color='#FFEC74'>Magicarpe</font>" + " !! Attrape le et remet le dans l'eau."));
            Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#FFEC74'>Magicarpe</font>"));
            Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_magicarpe_shiny);
        }
    }

    public void effetMagicarpe() {
        if (MainActivity.getEffetSonore()) {
            criMagicarpe.start();
        }
    }

    public void goBack(MenuItem m) {
        MainActivity.effetPopup();
        Log.d("PROJET LOG", "retour page 3");
        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    public void goRestart(MenuItem m) {
        MainActivity.effetPopup();
        Log.d("PROJET LOG", "retour page 3");
        startActivity(new Intent(this, MagicarpeActivity.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
        sensorManager.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }

    @Override
    public void onBackPressed() {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:
                return event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            case DragEvent.ACTION_DRAG_ENTERED:

            case DragEvent.ACTION_DRAG_EXITED:

            case DragEvent.ACTION_DROP:
                v.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                v.invalidate();
                MainActivity.effetPopup();
                if (event.getResult()) {
                    //Toast.makeText(this, "Bravo !", Toast.LENGTH_SHORT).show();
                    magicarpe.setVisibility(View.INVISIBLE);
                    lac.setImageResource(R.drawable.lac_reussi);
                    if (nbAleatoire == 1) {
                        txtMagicarpe.setText(Html.fromHtml("Bravo ! " + "<font color='#FFEC74'>Magicarpe</font>" + " a retrouvé ses amis."));
                    } else {
                        txtMagicarpe.setText(Html.fromHtml("Bravo ! " + "<font color='#EB7F62'>Magicarpe</font>" + " a retrouvé ses amis."));
                    }
                    effetWin();
                }
                else {
                    shinyMagicarpe();
                    //Toast.makeText(this, "Loupé...", Toast.LENGTH_SHORT).show();
                    txtMagicarpe.setText("Raté... Réessaye !");
                }
                return true;
            default:
                Log.e("PROJET LOG", "Erreur drag and drop");
                break;
        }
        return false;
    }

    public void shinyMagicarpe() {
        if (nbAleatoire == 1) {
            magicarpe.setImageResource(R.drawable.magicarpe_shiny);
            leviator.setImageResource(R.drawable.leviator_shiny);
            effetShiny();
        } else {
            magicarpe.setImageResource(R.drawable.magicarpe);
            leviator.setImageResource(R.drawable.leviator);
        }
        lancement = false;
    }


    @Override
    public boolean onLongClick(View v) {
        ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(v.getTag().toString(), mimeTypes, item);
        View.DragShadowBuilder dragshadow = new View.DragShadowBuilder(v);
        v.startDrag(data, dragshadow, v, 0);
        magicarpe.setImageResource(0);
        MainActivity.effetPopup();
        return true;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_magicarpe, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            x -= (int) event.values[0];
            y += (int) event.values[1];

            leviator.setY(y);
            leviator.setX(x);

        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}