package com.example.info0306_projet_antoine_duval.navigation;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.info0306_projet_antoine_duval.page1.CaptureActivity;
import com.example.info0306_projet_antoine_duval.page1.DeoxysActivity;
import com.example.info0306_projet_antoine_duval.page1.DialgaActivity;
import com.example.info0306_projet_antoine_duval.R;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    //En rapport avec la musique/les sons
    public static MediaPlayer theme;
    public static MediaPlayer popup;
    public static MediaPlayer bip;
    private static boolean effetSonore = true;
    private static boolean musiqueActivee = true;
    private static int numeroMusique = 2;

    //Premier lancement
    private static boolean started = false;

    //Sauvegarde de la capture
    private static int savePokemon = 0;

    //Sauvegarde du profil de l'utilisateur
    private static int numeroDresseur = 2;
    private static String pseudoDresseur = "Dresseur";

    //Sauvegarde de la photo prise
    private static Bitmap savePhoto;

    //Sauvegarde de l'état du quizz
    private static int numeroQuestion = 1;
    private static int score = 0;

    //Pour l'activité copain
    private static int pageCopain = 1;
    private static int pokemonSauvage = 0;

    //Pour deoxys
    private static int numeroDeoxys = 31;
    private static int niveauDeoxys = 20;

    private static SharedPreferences sharedPreferences;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        sharedPreferences= this.getSharedPreferences("DataPokemonCompanion", Context.MODE_PRIVATE);
        synchronisation();

        lancementMusique();

        Log.i("PROJET LOG", "Create main");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void openActivityDeoxys(View button)
    {
        effetPopup();
        Log.i("PROJET LOG", "openSecondActivity");
        startActivity(new Intent(this, DeoxysActivity.class));
        finish();
    }

    public void openActivityDialga(View button)
    {
        effetPopup();
        Log.i("PROJET LOG", "openActivityPokemon");
        startActivity(new Intent(this, DialgaActivity.class));
        finish();
    }

    public void openActivityCapture(View button)
    {
        effetPopup();
        Log.i("PROJET LOG", "openActivityCapture");
        startActivity(new Intent(this, CaptureActivity.class));
        finish();
    }

    public void openActivityTwo(View button)
    {
        effetPopup();
        Log.i("PROJET LOG", "page 2");
        startActivity(new Intent(this, SecondActivity.class));
        finish();
    }

    public void openActivityThree(View v)
    {
        Log.d("PROJET LOG", "page 3");
        effetPopup();
        startActivity(new Intent(this, ThirdActivity.class));
        finish();
    }

    public void synchronisation() {
        if(sharedPreferences != null) {
            pseudoDresseur = sharedPreferences.getString("Pseudo", "Dresseur");
            numeroDresseur = sharedPreferences.getInt("NumeroDresseur", 2);
            savePokemon = sharedPreferences.getInt("NumeroPokemon", 0);

            effetSonore = sharedPreferences.getBoolean("EffetSonore", true);
            musiqueActivee = sharedPreferences.getBoolean("Musique", true);
            numeroMusique = sharedPreferences.getInt("NumeroMusique", 2);
        } else {
            pseudoDresseur = "Dresseur";
            numeroDresseur = 2;
            savePokemon = 0;

            effetSonore = true;
            musiqueActivee = true;
            numeroMusique = 2;
        } sauvegarde();
    }

    public static void sauvegarde() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("Pseudo", pseudoDresseur);
        editor.putInt("NumeroDresseur", numeroDresseur);
        editor.putInt("NumeroPokemon", savePokemon);

        editor.putBoolean("EffetSonore", effetSonore);
        editor.putBoolean("Musique", musiqueActivee);
        editor.putInt("NumeroMusique", numeroMusique);

        editor.apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void lancementMusique() {
        if (!started) {
            switch(numeroMusique) {
                case 1:
                    theme = MediaPlayer.create(MainActivity.this, R.raw.theme_1);
                    break;
                case 2:
                    theme = MediaPlayer.create(MainActivity.this, R.raw.theme_2);
                    break;
                case 3:
                    theme = MediaPlayer.create(MainActivity.this, R.raw.theme_3);
                    break;
                case 4:
                    theme = MediaPlayer.create(MainActivity.this, R.raw.theme_4);
                    break;
                case 5:
                    theme = MediaPlayer.create(MainActivity.this, R.raw.theme_5);
                    break;
            }

            popup   =   MediaPlayer.create(MainActivity.this,R.raw.popup);
            bip     =   MediaPlayer.create(MainActivity.this,R.raw.bip);

            if(musiqueActivee) {
                theme.start();
            }

            Toast.makeText(this, "Bienvenue !!", Toast.LENGTH_SHORT).show();
            started         = true;
        } /*else {
            Toast.makeText(this, "Page 1", Toast.LENGTH_SHORT).show();
        }*/
    }

    public static void effetPopup() {
        if (effetSonore) {
            popup.start();
        }
    }

    public static void effetBip() {
        if (effetSonore) {
            bip.start();
        }
    }

    public static void setEffetSonore(boolean val)
    {
        effetSonore = val;
        sauvegarde();
    }

    public static boolean getEffetSonore() {return effetSonore; }

    public static void setMusiqueActivee(boolean val)
    {
        musiqueActivee = val;
        sauvegarde();
    }

    public static boolean getMusiqueActivee() { return  musiqueActivee; }

    public static Bitmap getSavePhoto() { return savePhoto; }

    public static void setSavePhoto(Bitmap photoPrise) { savePhoto = photoPrise; }

    public static void setSavePokemon(int save)
    {
        savePokemon = save;
        sauvegarde();
    }

    public static int getSavePokemon() {return savePokemon;}

    public static void setNumeroDresseur(int numero)
    {
        numeroDresseur = numero;
        sauvegarde();
    }

    public static int getNumeroDresseur() {return numeroDresseur;}

    public static void setNumeroMusique(int numero)
    {
        numeroMusique = numero;
        sauvegarde();
    }

    public static int getNumeroMusique() {return numeroMusique;}

    public static void setPseudoDresseur(String psd) {
        pseudoDresseur = psd;
        sauvegarde();
    }

    public static String getPseudoDresseur() {return pseudoDresseur;}

    public static int getNumeroQuestion() { return numeroQuestion; }

    public static void setNumeroQuestion(int num) { numeroQuestion = num; }

    public static int getScore() { return score; }

    public static void setScore(int val) { score = val; }

    public static int getPageCopain() { return pageCopain; }

    public static void setPageCopain(int page) { pageCopain = page; }

    public static int getPokemonSauvage() { return pokemonSauvage; }

    public static void setPokemonSauvage(int numero) { pokemonSauvage = numero; }

    public static int getNumeroDeoxys() { return numeroDeoxys; }

    public static void setNumeroDeoxys(int numero) { numeroDeoxys = numero; }

    public static int getNiveauDeoxys() { return niveauDeoxys; }

    public static void setNiveauDeoxys(int niveau) { niveauDeoxys = niveau; }

}