package com.example.info0306_projet_antoine_duval.page1;

import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.CompoundButtonCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.info0306_projet_antoine_duval.R;
import com.example.info0306_projet_antoine_duval.navigation.MainActivity;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class DialgaActivity extends AppCompatActivity {
    private int position = 1;
    private MediaPlayer cri;
    private MediaPlayer click;
    private ImageView imageDialga;
    private TextView txtDialga;
    private CheckBox checkBoxForme;
    private SeekBar seekBarArceus;
    private int numeroArceus = 0;
    private final int[][] states = {{android.R.attr.state_checked}, {}};
    private int[] colors;

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#DA70EF'>Les Divinités</font>"));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga);
        Objects.requireNonNull(getSupportActionBar()).setDisplayUseLogoEnabled(false);
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#2E2099"));
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
        setContentView(R.layout.activity_dialga);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE | SYSTEM_UI_FLAG_IMMERSIVE_STICKY | SYSTEM_UI_FLAG_HIDE_NAVIGATION   | SYSTEM_UI_FLAG_LAYOUT_STABLE | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        Log.i("Tp1", "Create pokemon");
        Log.i("Tp1", "Dialga");
        //Toast.makeText(this, "Dialga", Toast.LENGTH_SHORT).show();

        cri     =   MediaPlayer.create(DialgaActivity.this,R.raw.dialga);
        click   =   MediaPlayer.create(DialgaActivity.this,R.raw.click);
        if(MainActivity.getEffetSonore()) {
            cri.start();
        }

        imageDialga = findViewById(R.id.Dialga);
        imageDialga.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getX() < imageDialga.getWidth() / 2.0) {
                //Partie gauche de l'image
                backImage();
                Log.i("PROJET DIALGA", "Cliqué : gauche : image moins");
            } else {
                //Partie droite
                changeImage();
                Log.i("PROJET DIALGA", "Cliqué : droite : image plus");
            }
            return false;
        });

        txtDialga = findViewById(R.id.texteDialga);

        seekBarArceus = findViewById(R.id.seekBarArceus);
        seekBarArceus.setVisibility(View.INVISIBLE);
        seekBarArceus.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                numeroArceus = progress;
                changeArceus();
                Log.i("PROJET", "Progress changed seek bar arceus");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                MainActivity.effetPopup();
                Log.i("PROJET", "Start tracking seek bar arceus");
                //vide
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                crier();
                Log.i("PROJET", "Seek Bar Arceus :" + numeroArceus);
            }
        });

        checkBoxForme = findViewById(R.id.checkBoxForme);
        checkBoxForme.setChecked(false);
        colors = new int[]{getResources().getColor(R.color.dialga), getResources().getColor(R.color.dialga)};
        CompoundButtonCompat.setButtonTintList(checkBoxForme, new ColorStateList(states, colors));
        checkBoxForme.setOnCheckedChangeListener((buttonView, isChecked) -> {
            MainActivity.effetPopup();
            crier();
            if (isChecked) {
                imageDialga.setImageResource(R.drawable.dialga_o);
                checkBoxForme.setText("Forme Originelle");
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga_o);
            } else {
                imageDialga.setImageResource(R.drawable.dialga);
                checkBoxForme.setText("Forme Normale");
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga);
            }
            txtDialga.setText("Dialga");
        });
    }

    public void backImage() {
        Log.i("PROJET DIALGA", "Back image");
        switch(position) {
            case 1:
                position = 3;
                break;
            case 2:
                position = 16;
                break;
            case 3:
                position = 1;
                break;
            case 16:
                position = 2;
                break;
            case 4:
                position = 6;
                break;
            case 5:
                position = 7;
                break;
            case 6:
                position = 4;
                break;
            case 7:
                position = 5;
                break;
            case 8:
                position = 9;
                break;
            case 9:
                position = 10;
                break;
            case 10:
                position = 8;
                break;
            case 11:
                position = 14;
                break;
            case 12:
                position = 15;
                break;
            case 13:
                position = 11;
                break;
            case 14:
                position = 12;
                break;
            case 15:
                position = 13;
                break;
        }
        changeImage();
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    public void changeArceus() {
        switch (numeroArceus) {
            case 0:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#E3E1E0'>Normal</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_0_normal);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_0));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_0);
                break;
            case 1:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#75D543'>Plante</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_1_plante);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_1));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_1);
                break;
            case 2:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#D86C1B'>Feu</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_2_feu);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_2));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_2);
                break;
            case 3:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#328CE6'>Eau</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_3_eau);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_3));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_3);
                break;
            case 4:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#CDAC41'>Electrik</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_4_electrik);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_4));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_4);
                break;
            case 5:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#6888B4'>Vol</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_5_vol);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_5));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_5);
                break;
            case 6:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#92CD40'>Insecte</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_6_insecte);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_6));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_6);
                break;
            case 7:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#9D8D61'>Roche</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_7_roche);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_7));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_7);
                break;
            case 8:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#D84162'>Psy</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_8_psy);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_8));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_8);
                break;
            case 9:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#A045D1'>Poison</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_9_poison);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_9));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_9);
                break;
            case 10:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#6943CD'>Spectre</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_10_spectre);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_10));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_10);
                break;
            case 11:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#616161'>Tenèbres</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_11_tenebres);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_11));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_11);
                break;
            case 12:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#888888'>Acier</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_12_acier);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_12));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_12);
                break;
            case 13:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#AF4C31'>Combat</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_13_combat);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_13));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_13);
                break;
            case 14:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#46C0D2'>Glace</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_14_glace);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_14));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_14);
                break;
            case 15:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#CD8645'>Sol</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_15_sol);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_15));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_15);
                break;
            case 16:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#599D46'>Dragon</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_16_dragon);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_16));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_16);
                break;
            case 17:
                txtDialga.setText(Html.fromHtml("Arceus (<font color='#FF89B3'>Fée</font>)"));
                imageDialga.setImageResource(R.drawable.arceus_17_fee);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_17));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_17);
                break;
            case 18:
                txtDialga.setText(Html.fromHtml("<font color='#FFF782'>Arceus</font> (Shiny)"));
                imageDialga.setImageResource(R.drawable.arceus_18_shiny);
                seekBarArceus.setThumb(getResources().getDrawable(R.drawable.ic_arceus_18));
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_arceus_18);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    public void changeImage()
    {
        String[] listeNoms = {
                "Dialga", "Palkia", "Giratina",
                "Regirock", "Regice", "Registeel", "Regigigas",
                "Créhelf", "Créfollet", "Créfadet",
                "Heatran", "Darkrai", "Cresselia", "Shaymin", "Manaphy et Phione",
                "Arceus" };

        if(MainActivity.getEffetSonore()) {
            click.start();
        }

        if (position == 3) {
            seekBarArceus.setVisibility(View.VISIBLE);
        } else {
            seekBarArceus.setVisibility(View.INVISIBLE);
        }

        switch (position) {
            case 1:
                colors = new int[]{getResources().getColor(R.color.palkia), getResources().getColor(R.color.palkia)};
                checkBoxForme.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    MainActivity.effetPopup();
                    crier();
                    if (isChecked) {
                        imageDialga.setImageResource(R.drawable.palkia_o);
                        checkBoxForme.setText("Forme Originelle");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_palkia_o);
                    } else {
                        imageDialga.setImageResource(R.drawable.palkia);
                        checkBoxForme.setText("Forme Normale");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_palkia);
                    }
                });
                if (checkBoxForme.isChecked()) {
                    imageDialga.setImageResource(R.drawable.palkia_o);
                    checkBoxForme.setText("Forme Originelle");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_palkia_o);
                } else {
                    imageDialga.setImageResource(R.drawable.palkia);
                    checkBoxForme.setText("Forme Normale");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_palkia);
                }

                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.palkia);
                divinite();
                checkBoxForme.setVisibility(View.VISIBLE);
                break;
            case 2:
                colors = new int[]{getResources().getColor(R.color.giratina), getResources().getColor(R.color.giratina)};
                checkBoxForme.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    MainActivity.effetPopup();
                    crier();
                    if (isChecked) {
                        imageDialga.setImageResource(R.drawable.giratina_o);
                        checkBoxForme.setText("Forme Originelle");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_giratina_o);
                    } else {
                        imageDialga.setImageResource(R.drawable.giratina);
                        checkBoxForme.setText("Forme Alternative");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_giratina);
                    }
                });
                if (checkBoxForme.isChecked()) {
                    imageDialga.setImageResource(R.drawable.giratina_o);
                    checkBoxForme.setText("Forme Originelle");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_giratina_o);
                } else {
                    imageDialga.setImageResource(R.drawable.giratina);
                    checkBoxForme.setText("Forme Alternative");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_giratina);
                }

                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.giratina);
                divinite();
                checkBoxForme.setVisibility(View.VISIBLE);
                break;
            case 3:
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.arceus);
                changeArceus();
                position = 16;
                divinite();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 16:
                colors = new int[]{getResources().getColor(R.color.dialga), getResources().getColor(R.color.dialga)};
                checkBoxForme.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    MainActivity.effetPopup();
                    crier();
                    if (isChecked) {
                        imageDialga.setImageResource(R.drawable.dialga_o);
                        checkBoxForme.setText("Forme Originelle");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga_o);
                    } else {
                        imageDialga.setImageResource(R.drawable.dialga);
                        checkBoxForme.setText("Forme Normale");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga);
                    }
                });
                if (checkBoxForme.isChecked()) {
                    imageDialga.setImageResource(R.drawable.dialga_o);
                    checkBoxForme.setText("Forme Originelle");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga_o);
                } else {
                    imageDialga.setImageResource(R.drawable.dialga);
                    checkBoxForme.setText("Forme Normale");
                    Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_dialga);
                }

                position = 1;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.dialga);
                divinite();
                checkBoxForme.setVisibility(View.VISIBLE);
                break;
            case 4:
                imageDialga.setImageResource(R.drawable.regice);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.regice);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_regice);
                colosse();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 5:
                imageDialga.setImageResource(R.drawable.registeel);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.registeel);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_registeel);
                colosse();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 6:
                imageDialga.setImageResource(R.drawable.regigigas);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.regigigas);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_regigigas);
                colosse();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 7:
                imageDialga.setImageResource(R.drawable.regirock);
                position=4;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.regirock);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_regirock);
                colosse();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 8:
                imageDialga.setImageResource(R.drawable.crefollet);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.crefollet);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_crefollet);
                gardien();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 9:
                imageDialga.setImageResource(R.drawable.crefadet);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.crefadet);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_crefadet);
                gardien();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 10:
                imageDialga.setImageResource(R.drawable.crehelf);
                position=8;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.crehelf);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_crehelf);
                gardien();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 11:
                imageDialga.setImageResource(R.drawable.darkrai);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.darkrai);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_darkrai);
                fabuleux();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 12:
                imageDialga.setImageResource(R.drawable.cresselia);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.cresselia);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_cresselia);
                fabuleux();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 13:
                //Sinon il y a une mini animation de la checkBox non voulue à cause du listener de dialga (onCreate) :
                checkBoxForme.setChecked(false);
                imageDialga.setImageResource(R.drawable.shaymin);
                checkBoxForme.setText("Forme Terrestre");
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_shaymin);
                colors = new int[]{getResources().getColor(R.color.shaymin), getResources().getColor(R.color.shaymin)};
                checkBoxForme.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    MainActivity.effetPopup();
                    crier();
                    if (isChecked) {
                        imageDialga.setImageResource(R.drawable.shaymin_o);
                        checkBoxForme.setText("Forme Céleste");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_shaymin_c);
                    } else {
                        imageDialga.setImageResource(R.drawable.shaymin);
                        checkBoxForme.setText("Forme Terrestre");
                        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_shaymin);
                    }
                }); //Pas besoin de if apres car je veux que ce soit toujours la forme Terrestre quand on revient sur shaymin

                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.shaymin);
                fabuleux();
                checkBoxForme.setVisibility(View.VISIBLE);
                break;
            case 14:
                imageDialga.setImageResource(R.drawable.manaphy);
                position++;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.manaphy);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_manaphy);
                fabuleux();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
            case 15:
                imageDialga.setImageResource(R.drawable.heatran);
                position=11;
                cri = MediaPlayer.create(DialgaActivity.this, R.raw.heatran);
                Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_heatran);
                fabuleux();
                checkBoxForme.setVisibility(View.INVISIBLE);
                break;
        }

        CompoundButtonCompat.setButtonTintList(checkBoxForme, new ColorStateList(states, colors));

        if (position != 16) {
            txtDialga.setText(listeNoms[position-1]);
        }
        Log.i("LOG DIALGA", "set "+listeNoms[position-1]);
        //Toast.makeText(this, listeNoms[position-1], Toast.LENGTH_SHORT).show();
        crier();
    }

    public void divinite() {
        ConstraintLayout fond = findViewById(R.id.fenetreDialga);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#DA70EF'>Les Divinités</font>"));
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#2E2099"));
        fond.setBackgroundResource(R.drawable.background_dialga);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
    }

    public void colosse() {
        ConstraintLayout fond = findViewById(R.id.fenetreDialga);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#D7B268'>Les Colosses</font>"));
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#15351A"));
        fond.setBackgroundResource(R.drawable.background_regigias);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
    }

    public void gardien() {
        ConstraintLayout fond = findViewById(R.id.fenetreDialga);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#96E06A'>Les Gardiens</font>"));
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#324B6B"));
        fond.setBackgroundResource(R.drawable.background_crehelf);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
    }

    public void fabuleux() {
        ConstraintLayout fond = findViewById(R.id.fenetreDialga);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color='#DDBFA4'>Les Fabuleux</font>"));
        ColorDrawable couleurBarre = new ColorDrawable(Color.parseColor("#3F4063"));
        fond.setBackgroundResource(R.drawable.background_fabuleux);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(couleurBarre);
    }

    public void crier()
    {
        TimerTask taskDialga = new TimerTask() {
            public void run() {
                if(MainActivity.getEffetSonore()) {
                    cri.start();
                }
            }
        };
        Timer timerDialga = new Timer("timer");
        timerDialga.schedule(taskDialga, 200);
    }

    public void goMain(MenuItem m)
    {
        Log.d("PROJET", "retour main");
        MainActivity.effetPopup();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dialga, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    public void onClickMenu(MenuItem menuItem)
    {
        MainActivity.effetPopup();
    }

    public void onClickItemDialga(MenuItem m)
    {
        position = 16;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemPalkia(MenuItem m)
    {
        position = 1;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemGiratina(MenuItem m)
    {
        position = 2;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemArceus(MenuItem m)
    {
        position = 3;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemRegirock(MenuItem m)
    {
        position = 7;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemRegice(MenuItem m)
    {
        position = 4;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemRegisteel(MenuItem m)
    {
        position = 5;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemRegigigas(MenuItem m)
    {
        position = 6;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemCrehelf(MenuItem m)
    {
        position = 10;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemCrefollet(MenuItem m)
    {
        position = 8;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemCrefadet(MenuItem m)
    {
        position = 9;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemHeatran(MenuItem m)
    {
        position = 15;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemDarkrai(MenuItem m)
    {
        position = 11;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemCresselia(MenuItem m)
    {
        position = 12;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemShaymin(MenuItem m)
    {
        position = 13;
        changeImage();
        MainActivity.effetPopup();
    }

    public void onClickItemManaphy(MenuItem m)
    {
        position = 14;
        changeImage();
        MainActivity.effetPopup();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("PROJET LOG", "Start");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("PROJET LOG", "Resume");
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("PROJET LOG", "Pause");
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("PROJET LOG", "Stop");
    }
    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.i("PROJET LOG", "Restart");
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("PROJET LOG", "Destroy");
    }
    @Override
    public void onBackPressed()
    {
        MainActivity.effetPopup();
        super.onBackPressed();
        Log.i("PROJET LOG", "BackPressed");


        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}