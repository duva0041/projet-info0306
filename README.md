<img align="left" width="80" height="80" src="/compte_rendu/images/logo/pokedex.png" alt="Logo de l'application">

# Projet INFO0306

<br/>

## Antoine Duval S3F3

Application réalisée sur Android Studio : Pokemon Companion. <br/>
Cette application, sur l'univers des Pokémons, a pour but d'être un pokédex avec des mini-jeux.<br/>
Même sans connaître la licence il est très facile de s'en servir.

<br/>

![Image de présentation du projet](/compte_rendu/images/playstore/presentation.png "Image de présentation du projet")

<hr>
<br/> 

![Image de l'application](/compte_rendu/images/playstore/hand.png "Image de l'application")

